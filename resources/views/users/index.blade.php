@extends('layouts.app-template')

@section('assets-top')
<style type="text/css">
    
    .multiselect-selected-text{
        float: left;
    }

    .multiselect>.caret{
        float: right;
    }

    .dataTables_scrollBody{
        min-height: 51px;
    }
</style>
@endsection

@section('content')


<!-- BEGIN PAGE HEADER-->
<!-- BEGIN THEME PANEL -->

<!-- END THEME PANEL -->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Users</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> User Dashboard
    <small>statistics, charts, recent events and reports</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS 1-->
<!-- <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="1349">0</span>
                </div>
                <div class="desc"> New Feedbacks </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="#">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="12,5">0</span>M$ </div>
                <div class="desc"> Total Profit </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="#">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="549">0</span>
                </div>
                <div class="desc"> New Orders </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number"> +
                    <span data-counter="counterup" data-value="89"></span>% </div>
                <div class="desc"> Brand Popularity </div>
            </div>
        </a>
    </div>
</div> -->
<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->


<div class="portlet light ">
    <!-- PROJECT HEAD -->
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bar-chart font-green-sharp hide"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Data User</span>
            
        </div>

    </div>
    <!-- end PROJECT HEAD -->
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-8 col-sm-4">
                <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_3">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Role</th>                            
                            <th>&nbsp;</th>                            
                        </tr>
                    </thead>
                    
                </table>
            </div>
            <div class="todo-tasklist-devider"> </div>
            <div class="col-md-4 col-sm-8">
                @if(session('success'))
                <div class="alert alert-success display-show">
                <button class="close" data-close="alert"></button> {{ session('success') }} </div>
                @endif

                @if(session('error'))
                <div class="alert alert-danger display-show">
                <button class="close" data-close="alert"></button>  {{ session('error') }} </div>
                @endif
                <form id="myRoles" class="" role="form" action="{{ route('users.create') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                    <input type="hidden" class="form-control" name="id" id="user_id" value="0">    
                    <div class="form-body" style="height: 720px;">
                        
                        <div class="form-group form-md-line-input">
                            <input class="form-control" name="name" id="name_id" placeholder="Enter your name">
                            <label for="name_id">Nama
                                <span class="required">*</span>
                            </label>
                            <span class="help-block">Enter your name...</span>
                        </div>

                        <div class="form-group form-md-line-input">
                            <input class="form-control" name="email" id="email_id" placeholder="Enter your email">
                            <label for="email_id">Email
                                
                            </label>
                            <span class="help-block">Enter your email...</span>
                        </div>

                        <div class="form-group form-md-line-input">
                            <input class="form-control" name="phone" id="phone_id" placeholder="Enter your phone number">
                            <label for="phone_id">Phone Number
                                <span class="required">*</span>
                            </label>
                            <span class="help-block">Enter your phone number...</span>
                        </div>

                        <div class="form-group form-md-line-input">
                            <input class="form-control" name="nik" id="nik_id" placeholder="Enter your NIK">
                            <label for="nik_id">NIK
                                
                            </label>
                            <span class="help-block">Enter your NIK...</span>
                        </div>

                        <div class="form-group form-md-line-input">
                            <textarea class="form-control" id="alamat_id" name="alamat" rows="3"></textarea>
                            <label for="alamat_id">Alamat</label>
                            <span class="help-block">Enter your address...</span>
                        </div>

                        <hr>

                        <div class="form-group form-md-line-input">
                            <input type="password" class="form-control" name="password" id="password_id" placeholder="Enter your password" value="12345">
                            <label for="password_id">Password (Defauld:12345)
                                <span class="required">*</span>
                            </label>
                            <span class="help-block">Enter your password...</span>
                        </div>
                       <!--  <div class="form-group form-md-line-input">
                            <input class="form-control" id="form_control_1" name="number" placeholder="Enter number">
                            <label for="form_control_1">Phone Number</label>
                        </div> -->
                        
                        <div class="form-group form-md-line-input">
                            <select class="form-control" id="roles_id" name="roles_id">
                                @foreach($rl as $key):
                                    <option value="{{$key->id}}">{{$key->name}}</option>
                                @endforeach
                            </select>
                            <label for="form_control_1">Role</label>
                            <span class="help-block">Please enter role</span>
                        </div>
                        
                    </div>

                    <div class="form-actions right">
                        <button type="submit" class="btn green btn-sm">Submit</button>
                        <button type="reset" class="btn default btn-sm">Reset</button>
                        <a id="delete_btn" class="btn red-mint btn-outline sbold uppercase btn-sm">Delete</a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection


@section('assets-bottom')
<script>
$(document).ready(function() {

    $("#delete_btn").hide();
        var form1 = $('#myRoles');
    var error1 = $('.alert-danger', form1);
    var success1 = $('.alert-success', form1);

    form1.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input

        rules: {
            name: {
                required: true
            },

            email: {
                email: true
            },

            phone: {
                required: true,
                digits: true
            },

            nik: {
                digits: true
            },


        },

        invalidHandler: function(event, validator) { //display error alert on form submit              
            success1.hide();
            error1.show();
            App.scrollTo(error1, -200);
        },

        errorPlacement: function(error, element) {
            if (element.is(':checkbox')) {
                error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
            } else if (element.is(':radio')) {
                error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        highlight: function(element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function(element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function(label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function(form) {
            success1.show();
            error1.hide();
            form1[0].submit(); // submit the form

        }
    });

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    var table = $('#sample_3');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{ route('users.list') }}",                  
    },
    // Or you can use remote translation file
    //"language": {
    //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
    //},

    // setup buttons extension: http://datatables.net/extensions/buttons/
    // buttons: [
    //     { extend: 'print', className: 'btn dark btn-outline' },
    //     { extend: 'pdf', className: 'btn green btn-outline' },
    //     { extend: 'csv', className: 'btn purple btn-outline ' }
    // ],

    // scroller extension: http://datatables.net/extensions/scroller/
    scrollY:        400,
    deferRender:    true,
    scroller:       true,
    deferRender:    true,
    scrollX:        true,
    scrollCollapse: true,        
    responsive:     true,  
    stateSave:      true,
    columnDefs: [
            { orderable: false, targets: [5] },
            { searchable : false},                
            { render: function(data, type, row, meta) {

                return '<a href="javascript:;" data-id="'+data[0]+'" data-name="'+data[1]+'" data-email="'+data[2]+'" data-phone="'+data[3]+'" data-nik="'+data[4]+'" data-alamat="'+data[5]+'" data-roleid="'+data[7]+'" class="edit_btn btn btn-circle btn-icon-only grey-mint"><i class="fa fa-edit"></i></a>'; }, targets: [5]
            },
    ],
    columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'phone', name: 'phone' },
            { data: 'roles', name: 'roles' }, 
            { data: 'users_id', name: 'users_id' } 
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
    // So when dropdowns used the scrollable div should be removed. 
    //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
    });

    $("#sample_3").on('click','.edit_btn',function(e){
        var id = e.currentTarget.dataset.id;
        var name = e.currentTarget.dataset.name;
        var email = e.currentTarget.dataset.email=='null'?'':e.currentTarget.dataset.email;
        var phone = e.currentTarget.dataset.phone=='null'?'':e.currentTarget.dataset.phone;
        var nik = e.currentTarget.dataset.nik=='null'?'':e.currentTarget.dataset.nik;
        var alamat = e.currentTarget.dataset.alamat=='null'?'':e.currentTarget.dataset.alamat;
        var roleid = e.currentTarget.dataset.roleid;



        $("#user_id").val(id);
        $("#name_id").val(name);
        $("#email_id").val(email);
        $("#phone_id").val(phone);
        $("#nik_id").val(nik);
        $("#alamat_id").val(alamat);
        $("#password_id").val('');
        $("#roles_id").val(roleid);
        $("#delete_btn").show();
    });

    $("#delete_btn").on('click',function(e){
        var id = $("#user_id").val();
        console.log(id);
        //console.log(sa_btnClass);
        swal({
          title: "Apakah akan menghapus Role User Ini?",
          text: "Role User akan dihapus dari web, apakah anda yakin akan melakukan proses ini...",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Setuju hapus",
          cancelButtonText: "Tidak, Batal hapus",
        },function(isConfirm){
                if (isConfirm){
                    $.ajax({
                        url: "{{ url('users-delete') }}/"+id,
                        type: "GET",
                        dataType: "JSON",
                        success: function(data) {
                            if(data){
                                swal("Terimakasih", "Anda telah menghapus data", "success");
                            }else{
                                swal("Maaf!!!", "User Tidak Memiliki akses Hapus", "error");
                            }
                            
                            location.reload(true);
                        }
                    });
                    
                } else {
                    swal("Batal", "Anda membatalkan proses hapus client...", "error");
                }
        });
    });

    
})

</script>
@endsection    