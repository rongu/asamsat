<h3>Halo, {{ $nama }} !</h3>
<p>Antrian Pelayanan <b>{{ $nama_pelayanan }}</b> Anda dengan nomor Antrian <b>{{ $kode_antrian }} - {{ $next_urut }}</b> akan segera dilayani.</p>
<p>Silahkan segera menunggu di tempat mengantri yang sudah kami sediakan. Atas Perhatiannya kami ucapkan terima kasih.</p>
<br>
<p>Salam Hormat,</p>
<p>Kepala Kantor Samsat Bengkulu Utara</p>