<h3>Halo, {{ $nama }} !</h3>
<p>Terima kasih telah melakukan pendaftaran pada website <a href="https://www.samsatbengkuluutara.com">www.samsatbengkuluutara.com</a></p>
<p>Akses Akun Anda dengan :</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Username : {{ $username }}</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Password : {{ $password }}</p>
<p>Anda bisa mengambil antrian untuk semua jenis pelayanan yang tersedia di Samsat Bengkulu Utara melalui Website ini.</p>
<br>
<p>Salam Hormat,</p>
<p>Kepala Kantor Samsat Bengkulu Utara</p>