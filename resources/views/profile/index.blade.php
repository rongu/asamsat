@extends('layouts.app-template')


@section('assets-top')

<style type="text/css">
    
    .dataTables_scrollBody{
        min-height: 51px;
    }
</style>

@endsection  

@section('content')


<!-- BEGIN PAGE HEADER-->
<!-- BEGIN THEME PANEL -->

<!-- END THEME PANEL -->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Profile</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Profile Dashboard
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS 1-->

<div class="profile">
    <div class="tabbable-line tabbable-full-width">
        <div class="tab-content">
            <!--tab_1_2-->
            <div class="tab-pane active" id="tab_1_3">
                @if(session('success'))
                <div class="alert alert-success display-show">
                <button class="close" data-close="alert"></button> {{ session('success') }} </div>
                @endif

                @if(session('error'))
                <div class="alert alert-danger display-show">
                <button class="close" data-close="alert"></button>  {{ session('error') }} </div>
                @endif
                <form id="myProfil" class="" role="form" action="{{route('profil.create')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                
                <div class="row profile-account">
                    <div class="col-md-3">
                        <ul class="ver-inline-menu tabbable margin-bottom-10">
                            <li class="active">
                                <a data-toggle="tab" href="#tab_1-1">
                                    <i class="fa fa-cog"></i> Personal info </a>
                                <span class="after"> </span>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_3-3">
                                    <i class="fa fa-lock"></i> Password </a>
                            </li>
                            
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">
                                
                                <input id="employee_id" type="hidden" class="form-control" name="id" value="{{$user->id}}">
                                <div class="form-body">
                                    
                                    <div class="form-group form-md-line-input">
                                        <input type="text" class="form-control" name="nik_em" id="nik_em_id" value="{{$user->nik}}">
                                        <label for="nik_em_id">NIK
                                            <span class="required">*</span>
                                        </label>
                                        <span class="help-block">Enter your employee NIK...</span>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <input type="text" class="form-control" name="name_em" id="name_em_id" value="{{$user->name}}">
                                        <label for="name_em_id">Nama
                                            <span class="required">*</span>
                                        </label>
                                        <span class="help-block">Enter your employee name...</span>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <input type="email" class="form-control" name="email_em" id="email_em_id" value="{{$user->email}}">
                                        <label for="email_em_id">Email
                                            <span class="required">*</span>
                                        </label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <input type="text" class="form-control" name="phones_em" id="phones_em_id" value="{{$user->phone}}">
                                        <label for="pic_no_id">Nomor Telepon
                                            <span class="required">*</span>
                                        </label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <textarea class="form-control" name="address_em" rows="4" id="address_em_id">{{$user->alamat}}</textarea>
                                        <label for="address_em_id">Alamat</label>
                                    </div>                                            
                                </div>
                            </div>
                            <div id="tab_3-3" class="tab-pane">
                                    
                                    <div class="form-group form-md-line-input">
                                        <input type="password" class="form-control" id="password_2_id" name="password_2" value="">
                                        <label for="pic_name_id">New Password</label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <input type="password" class="form-control" id="password_3_id" name="password_3" value="">
                                        <label for="pic_name_id">Re-type Password</label>
                                    </div>
                            </div>
                            
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>
                <div class="form-actions right">
                        <button type="submit" class="btn green btn-sm">Submit</button>
                        <button type="reset" class="btn default btn-sm">Reset</button>
                        
                    </div>
                </form>
            </div>
            <!--end tab-pane-->
            
        </div>
    </div>
</div>

@endsection