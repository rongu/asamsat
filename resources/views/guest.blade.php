@extends('layouts.guest-template')

@section('content')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Dashboard</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bubble font-dark hide"></i>
                        <span class="caption-subject font-hide bold uppercase">Daftar Antrian</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6">
                            <!--begin: widget 1-1 -->
                            <div class="mt-widget-1">
                                <div>&nbsp;</div>
                                <div><h4>{{date('d-M-Y')}}</h4></div>
                                <div class="mt-body">
                                    <center>
                                    <h2 style="font-family: verdana;" id="jam"></h2>
                                    </center>                                    
                                </div>
                            </div>
                            <!--end: widget 1-1 -->
                        </div>         
                        <div class="col-md-6">
                            <!--begin: widget 1-2 -->
                            <div class="mt-widget-1">
                                <div>&nbsp;</div>
                                <div><h4>Pelayanan Perekaman Sidik Jari</h4></div>
                                <div class="mt-body">
                                    @if ($sidik_jari)
                                    <h2><span id="nomor_antrian_sidik_jari">{{$sidik_jari->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sidik_jari->nomor_antrian)}}</span></h2>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">
                                            <a id="btn-ambil-sidik-jari" data-mdid="{{$sidik_jari->md_pelayanan_id}}" data-tanggal="{{$sidik_jari->tanggal_pelayanan}}" data-antrian="{{$sidik_jari->nomor_antrian}}" class="btn font-green">
                                                <i class="fa fa-print"></i> Ambil Antrian</a>
                                        </div>
                                    </div>
                                    @else
                                    <h2><span id="nomor_antrian_sidik_jari"> - </span></h2>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">    
                                            <a id="btn-ambil-sidik-jari" data-mdid="5" data-tanggal="tanggal" data-antrian="1" class="btn font-green">
                                                <i class="fa fa-print"></i> Ambil Antrian</a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <!--end: widget 1-2 -->
                        </div>
                        <div class="col-md-12"><br></div>
                        <div class="col-md-6">
                            <!--begin: widget 1-3 -->
                            <div class="mt-widget-1">
                                <div>&nbsp;</div>
                                <div><h4>Pelayanan SIM BARU</h4></div>
                                <div class="mt-body">
                                    @if ($sim_baru)
                                    <h2><span id="nomor_antrian_sim_baru">{{$sim_baru->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sim_baru->nomor_antrian)}}</span></h2>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">    
                                            <a id="btn-ambil-sim-baru" data-mdid="{{$sim_baru->md_pelayanan_id}}" data-tanggal="{{$sim_baru->tanggal_pelayanan}}" data-antrian="{{$sim_baru->nomor_antrian}}" class="btn font-green">
                                                <i class="fa fa-print"></i> Ambil Antrian</a>
                                        </div>
                                    </div>
                                    @else
                                    <h2><span id="nomor_antrian_sim_baru"> - </span></h2>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">    
                                            <a id="btn-ambil-sim-baru" data-mdid="1" data-tanggal="tanggal" data-antrian="1" class="btn font-green">
                                                <i class="fa fa-print"></i> Ambil Antrian</a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <!--end: widget 1-3 -->
                        </div>
                        
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                            <!--begin: widget 1-3 -->
                            <div class="mt-widget-1">
                                <div>&nbsp;</div>
                                <div><h4>Pelayanan SIM Perpanjang</h4></div>
                                <div class="mt-body">
                                    @if ($sim_perpanjang)
                                    <h2><span id="nomor_antrian_sim_perpanjang">{{$sim_perpanjang->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sim_perpanjang->nomor_antrian)}}</span></h2>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">     
                                            <a id="btn-ambil-sim-perpanjang" data-mdid="{{$sim_perpanjang->md_pelayanan_id}}" data-tanggal="{{$sim_perpanjang->tanggal_pelayanan}}" data-antrian="{{$sim_perpanjang->nomor_antrian}}" class="btn font-green">
                                                <i class="fa fa-print"></i> Ambil Antrian</a>
                                        </div>
                                    </div>
                                    @else
                                    <h2><span id="nomor_antrian_sim_perpanjang"> - </span></h2>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">   
                                            <a id="btn-ambil-sim-perpanjang" data-mdid="2" data-tanggal="tanggal" data-antrian="1" class="btn font-green">
                                                <i class="fa fa-print"></i> Ambil Antrian</a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                            <!--end: widget 1-3 -->
                        </div>
                        <div class="col-md-12"><br></div>
                        <div class="col-md-6">
                            <!--begin: widget 1-3 -->
                            <div class="mt-widget-1">
                                <div>&nbsp;</div>
                                <div><h4>Pelayanan SKCK BARU</h4></div>
                                <div class="mt-body">
                                    @if ($skck_baru)
                                    <h2><span id="nomor_antrian_skck_baru">{{$skck_baru->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($skck_baru->nomor_antrian)}}</span></h2>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">     
                                            <a id="btn-ambil-skck-baru" data-mdid="{{$skck_baru->md_pelayanan_id}}" data-tanggal="{{$skck_baru->tanggal_pelayanan}}" data-antrian="{{$skck_baru->nomor_antrian}}" class="btn font-green">
                                                <i class="fa fa-print"></i> Ambil Antrian</a>
                                        </div>
                                    </div>
                                    @else
                                    <h2><span id="nomor_antrian_skck_baru"> - </span></h2>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">    
                                            <a id="btn-ambil-skck-baru" data-mdid="3" data-tanggal="tanggal" data-antrian="1" class="btn font-green">
                                                <i class="fa fa-print"></i> Ambil Antrian</a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <!--end: widget 1-3 -->
                        </div>
                        <div class="col-md-6">
                            <!--begin: widget 1-3 -->
                            <div class="mt-widget-1">
                                <div>&nbsp;</div>
                                <div><h4>Pelayanan SKCK Perpanjang</h4></div>
                                <div class="mt-body">
                                    @if ($skck_perpanjang)
                                    <h2><span id="nomor_antrian_skck_perpanjang">{{$skck_perpanjang->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($skck_perpanjang->nomor_antrian)}}</span></h2>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">    
                                            <a id="btn-ambil-skck-perpanjang" data-mdid="{{$skck_perpanjang->md_pelayanan_id}}" data-tanggal="{{$skck_perpanjang->tanggal_pelayanan}}" data-antrian="{{$skck_perpanjang->nomor_antrian}}" class="btn font-green">
                                                <i class="fa fa-print"></i> Ambil Antrian</a>
                                        </div>
                                    </div>
                                    @else
                                    <h2><span id="nomor_antrian_skck_perpanjang"> - </span></h2>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">    
                                            <a id="btn-ambil-skck-perpanjang" data-mdid="4" data-tanggal="tanggal" data-antrian="1" class="btn font-green">
                                                <i class="fa fa-print"></i> Ambil Antrian</a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <!--end: widget 1-3 -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hover_bkgr_fricc">
        <span class="helper"></span>
        <div>
            <div class="popupCloseButton">&times;</div>
              <div class="receipt" id="printableArea" style="background-image: url('{{ asset('images/splash.png') }}');background-repeat: no-repeat;background-size: 100% 100%;">
                <div class="ticketTitle">
                  <span>SAMSAT BENGKULU UTARA</span>
                </div>
                <div class="ticketAlamat">
                  <span>Jl. Padang - Bengkulu, Ps. Baru Kota Bani, Putri Hijau,<br>Kabupaten Bengkulu Utara, Bengkulu 38326 </span>
                </div>
                <div class="headerSubTitle">
                  <span id="print_">NOMOR ANTRIAN</span>
                </div>
                <div class="headerTitle">
                  <span id="print_nomor_pelayanan"></span>
                </div>
                <div class="headerSubTitle">
                  <span id="print_jenis_pelayanan"></span>
                </div>
                <div class="keepIt">
                  Simpan Tiket Anda!
                </div>
                <div class="keepItBody">
                  <center>Terimakasih Atas Kunjungan Anda.</center>
                </div>     
                <div id="date">
                 <span id="print_waktu">{{date("d-m-Y H:i:s")}}</span> WIB
                </div>
              </div>
        </div>
    </div>

@endsection

@section('assets-bottom')
<script type="text/javascript">  
    $(window).load(function () {
        $(".trigger_popup_fricc").click(function(){
           $('.hover_bkgr_fricc').show();
        });
        $('.hover_bkgr_fricc').click(function(){
            $('.hover_bkgr_fricc').hide();
        });
        $('.popupCloseButton').click(function(){
            $('.hover_bkgr_fricc').hide();
        });
    });

    function printDiv(divName) {
         var printContents = document.getElementById(divName).innerHTML;
         var originalContents = document.body.innerHTML;

         document.body.innerHTML = printContents;

         window.print();

         document.body.innerHTML = originalContents;
    }

     window.onload = function() { jam(); }

     function jam() {
      var e = document.getElementById('jam'),
      d = new Date(), h, m, s;
      h = d.getHours();
      m = set(d.getMinutes());
      s = set(d.getSeconds());

      e.innerHTML = h +':'+ m +':'+ s;

      setTimeout('jam()', 1000);
     }

     function set(e) {
      e = e < 10 ? '0'+ e : e;
      return e;
     }


    $('#btn-ambil-sidik-jari').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan mengambil antian pelayanan"+"\n"+"sidik jari?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('guest-ambil-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        $('#nomor_antrian_sidik_jari').text(result.kode_antrian);
                        $('#total_antrian_sidik_jari').text(result.total);
                        $('#btn-ambil-sidik-jari').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-ambil-sidik-jari').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-ambil-sidik-jari').attr('data-antrian',result.nomor_antrian);
                        $('#print_nomor_pelayanan').text(result.kode_antrian);
                        $('#print_jenis_pelayanan').text(result.nama_pelayanan);
                      }
                })
                printDiv('printableArea');
                swal({
                  title: "Cetak Tiket Antrian!",
                  text: "",
                  type: "success",
                  allowOutsideClick: false,
                  showConfirmButton: true,
                  confirmButtonClass: "btn-success",
                  closeOnConfirm: true,
                  confirmButtonText: "OK",
                },function(isConfirm){
                    if (isConfirm){
                        swal("Berhasil", "Antrian Sedang dicetak.", "success"); 
                        location.reload(); 
                    }
                });
            } else {
                swal("Batal", "Anda membatalkan cetak nomor antrian.", "error");
            }
        });        
    });

    $('#btn-ambil-sim-baru').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan mengambil antian pelayanan"+"\n"+"sim baru?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('guest-ambil-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        $('#nomor_antrian_sim_baru').text(result.kode_antrian);
                        $('#total_antrian_sim_baru').text(result.total);
                        $('#btn-ambil-sim-baru').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-ambil-sim-baru').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-ambil-sim-baru').attr('data-antrian',result.nomor_antrian);
                        $('#print_nomor_pelayanan').text(result.kode_antrian);
                        $('#print_jenis_pelayanan').text(result.nama_pelayanan);
                      }
                })
                printDiv('printableArea');
                swal({
                  title: "Cetak Tiket Antrian!",
                  text: "",
                  type: "success",
                  allowOutsideClick: false,
                  showConfirmButton: true,
                  confirmButtonClass: "btn-success",
                  closeOnConfirm: true,
                  confirmButtonText: "OK",
                },function(isConfirm){
                    if (isConfirm){
                        swal("Berhasil", "Antrian Sedang dicetak.", "success"); 
                        location.reload(); 
                    }
                });    
            } else {
                swal("Batal", "Anda membatalkan cetak nomor antrian.", "error");
            }
        });        
    });

    $('#btn-ambil-sim-perpanjang').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan mengambil antian pelayanan"+"\n"+"sim perpanjang?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('guest-ambil-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        $('#nomor_antrian_sim_perpanjang').text(result.kode_antrian);
                        $('#total_antrian_sim_perpanjang').text(result.total);
                        $('#btn-ambil-sim-perpanjang').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-ambil-sim-perpanjang').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-ambil-sim-perpanjang').attr('data-antrian',result.nomor_antrian);
                        $('#print_nomor_pelayanan').text(result.kode_antrian);
                        $('#print_jenis_pelayanan').text(result.nama_pelayanan);
                      }
                })
                printDiv('printableArea');
                swal({
                  title: "Cetak Tiket Antrian!",
                  text: "",
                  type: "success",
                  allowOutsideClick: false,
                  showConfirmButton: true,
                  confirmButtonClass: "btn-success",
                  closeOnConfirm: true,
                  confirmButtonText: "OK",
                },function(isConfirm){
                    if (isConfirm){
                        swal("Berhasil", "Antrian Sedang dicetak.", "success"); 
                        location.reload(); 
                    }
                });     
            } else {
                swal("Batal", "Anda membatalkan cetak nomor antrian.", "error");
            }
        });        
    });

    $('#btn-ambil-skck-baru').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan mengambil antian pelayanan"+"\n"+"skck baru?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('guest-ambil-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        $('#nomor_antrian_skck_baru').text(result.kode_antrian);
                        $('#total_antrian_skck_baru').text(result.total);
                        $('#btn-ambil-skck-baru').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-ambil-skck-baru').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-ambil-skck-baru').attr('data-antrian',result.nomor_antrian);
                        $('#print_nomor_pelayanan').text(result.kode_antrian);
                        $('#print_jenis_pelayanan').text(result.nama_pelayanan);
                      }
                })
                printDiv('printableArea');
                swal({
                  title: "Cetak Tiket Antrian!",
                  text: "",
                  type: "success",
                  allowOutsideClick: false,
                  showConfirmButton: true,
                  confirmButtonClass: "btn-success",
                  closeOnConfirm: true,
                  confirmButtonText: "OK",
                },function(isConfirm){
                    if (isConfirm){
                        swal("Berhasil", "Antrian Sedang dicetak.", "success"); 
                        location.reload(); 
                    }
                });       
            } else {
                swal("Batal", "Anda membatalkan cetak nomor antrian.", "error");
            }
        });        
    });

    $('#btn-ambil-skck-perpanjang').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan mengambil antian pelayanan"+"\n"+"skck perpanjang?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('guest-ambil-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        $('#nomor_antrian_skck_perpanjang').text(result.kode_antrian);
                        $('#total_antrian_skck_perpanjang').text(result.total);
                        $('#btn-ambil-skck_perpanjang').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-ambil-skck_perpanjang').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-ambil-skck_perpanjang').attr('data-antrian',result.nomor_antrian);
                        $('#print_nomor_pelayanan').text(result.kode_antrian);
                        $('#print_jenis_pelayanan').text(result.nama_pelayanan);
                      }
                })
                printDiv('printableArea');
                swal({
                  title: "Cetak Tiket Antrian!",
                  text: "",
                  type: "success",
                  allowOutsideClick: false,
                  showConfirmButton: true,
                  confirmButtonClass: "btn-success",
                  closeOnConfirm: true,
                  confirmButtonText: "OK",
                },function(isConfirm){
                    if (isConfirm){
                        swal("Berhasil", "Antrian Sedang dicetak.", "success"); 
                        location.reload(); 
                    }
                });
            } else {
                swal("Batal", "Anda membatalkan cetak nomor antrian.", "error");
            }
        });

        
    });
</script>

@endsection    