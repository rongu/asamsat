@extends('layouts.app-template')

@section('assets-top')
<style type="text/css">
    
    .multiselect-selected-text{
        float: left;
    }

    .multiselect>.caret{
        float: right;
    }

    .dataTables_scrollBody{
        min-height: 51px;
    }
</style>
@endsection

@section('content')


<!-- BEGIN PAGE HEADER-->
<!-- BEGIN THEME PANEL -->

<!-- END THEME PANEL -->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Users</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Rekap Pendaftaran Dashboard
    <small>statistics, charts, recent events and reports</small>
</h1>

<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->


<div class="portlet light ">
    <!-- PROJECT HEAD -->
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bar-chart font-green-sharp hide"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Rekap Pendaftaran</span>
        </div>
        <div class="actions form-inline">
            <select class="form-control" id="filter_jenis_pelayanan_id" name="filter_jenis_pelayanan">
                <option value="0">TAMPILKAN SEMUA</option>
                @foreach($jpl as $val)
                    <option value="{{$val->id}}">{{$val->nama_pelayanan}}</option>
                @endforeach
            </select>
            <input type="text" id="filter_tanggal_pelayanan_id" name="filter_tanggal_pelayanan" class="date-picker form-control text-center">
        </div>

    </div>
    <!-- end PROJECT HEAD -->
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_3">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Nomor Telepon</th>
                            <th>Jenis Pelayanan</th>               
                            <th>Nomor Urut</th>                            
                            <th>Status Pelayanan</th>                            
                        </tr>
                    </thead>
                    
                </table>
            </div>
        </div>
    </div>
</div>

@endsection


@section('assets-bottom')
<script>
$(document).ready(function() {

    $("#filter_tanggal_pelayanan_id").datepicker("setDate", new Date());

    var form1 = $('#myRoles');
    var error1 = $('.alert-danger', form1);
    var success1 = $('.alert-success', form1);

    form1.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input

        rules: {
            name: {
                required: true
            },

            email: {
                email: true
            },

            phone: {
                required: true,
                digits: true
            },

            nik: {
                digits: true
            },


        },

        invalidHandler: function(event, validator) { //display error alert on form submit              
            success1.hide();
            error1.show();
            App.scrollTo(error1, -200);
        },

        errorPlacement: function(error, element) {
            if (element.is(':checkbox')) {
                error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
            } else if (element.is(':radio')) {
                error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        highlight: function(element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function(element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function(label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function(form) {
            success1.show();
            error1.hide();
            form1[0].submit(); // submit the form

        }
    });

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    var table = $('#sample_3');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{ route('rekap.list') }}",
        "type": "POST",
        "data" :  function(d) {
                d.tanggal_pelayanan = $("#filter_tanggal_pelayanan_id").val();
                d.jenis_pelayanan = $("#filter_jenis_pelayanan_id").val();
            }                   
    },
    // Or you can use remote translation file
    //"language": {
    //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
    //},

    // setup buttons extension: http://datatables.net/extensions/buttons/
    // buttons: [
    //     { extend: 'print', className: 'btn dark btn-outline' },
    //     { extend: 'pdf', className: 'btn green btn-outline' },
    //     { extend: 'csv', className: 'btn purple btn-outline ' }
    // ],

    // scroller extension: http://datatables.net/extensions/scroller/
    scrollY:        400,
    deferRender:    true,
    scroller:       true,
    deferRender:    true,
    scrollX:        true,
    scrollCollapse: true,        
    responsive:     false,  
    stateSave:      true,
    columnDefs: [
            { orderable: false, targets: [5] },
            { searchable : false},     
    ],
    columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'nama', name: 'nama' },
            { data: 'telepon', name: 'telepon' },
            { data: 'nama_pelayanan', name: 'nama_pelayanan' },
            { data: 'nomor_antrian', name: 'nomor_antrian' },
            { data: 'status_pelayanan', name: 'status_pelayanan' }
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
    // So when dropdowns used the scrollable div should be removed. 
    //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
    });

    $("#filter_jenis_pelayanan_id").on("change",function(){
        oTable.fnDraw();        
    });

    $("#filter_tanggal_pelayanan_id").on("change",function(){
        oTable.fnDraw();        
    });
    
})

</script>
@endsection    