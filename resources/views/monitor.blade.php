<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Monitor</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->

        <link href="public/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="public/assets/layouts/layout5/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/layouts/layout5/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    @if ($av)
        @php
            $link_video = $av->link_video;
            $footnote = $av->footnote;
        @endphp
    @endif

    <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
            <!-- BEGIN HEADER -->
            <header class="page-header">
                <nav class="navbar mega-menu" role="navigation" style="min-height: 145px;">
                    <div class="container-fluid">
                        <div class="clearfix navbar-fixed-top">

                            <a id="index" class="page-logo" style="margin-right:0px;height: 105px;max-height: 106px;" ><img src="public/images/logoheader.png" alt="Logo"></a>
                            <span style="font-size: 3.5em;color: #ffffff;">Polres Bengkulu Utara</span><br>
                            <span style="font-size: 1em;color: #ffffff;">Melayani dengan sepenuh hati</span>
                            <div class="topbar-actions">
                                <!-- BEGIN GROUP NOTIFICATION -->
                                <div class="btn-group-notification btn-group" id="header_notification_bar" style="text-align: center;">
                                    <span class="label label-success" id="jam" style="font-size: 4em;background-color: #2f373e;">00:00:00</span><br><br>
                                    <span class="label label-success" style="font-size: 2em;background-color: #2f373e;">{{date('d-M-Y')}}</span>                     
                                </div>

                            </div>
                        </div>
                        
                    </div>
                    
                </nav>
            </header>
            <!-- END HEADER -->
            <div class="container-fluid">
                <div class="page-content" style="padding-top: 0px;padding: 10px;margin-top: 0px;margin-bottom: 0px">

                <div class="row">
                    <div class="col-7 col-md-7 col-sm-7">
                        <div id="vh" style="height: 200px;position: relative;background-color: #000000;">

                            <div style=" margin: 0;position: absolute;top: 50%; -ms-transform: translateY(-50%);transform: translateY(-50%);">
                                <video controls autoplay loop muted style="width: 100%!important;height: auto   !important;"> 
                              <source src="public/video/{{$link_video}}" type="video/mp4">
                            </video> 
                            </div>
                               
                        </div>
                        
                    </div>
                    <div class="col-5 col-md-5 col-sm-5">
                        <!--begin: widget 1-2 -->
                        <div class="mt-widget-1 bg-blue-sharp bg-font-blue-sharp">
                            <div>&nbsp;</div>
                            <div><h4>Pelayanan Perekaman Sidik Jari</h4></div>
                            <div class="mt-body">
                                @if ($sidik_jari)
                                <h2><span style="font-weight:bold;" id="nomor_antrian_sidik_jari">{{$sidik_jari->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sidik_jari->nomor_antrian)}}</span></h2>
                                @else
                                <h2><span style="font-weight:bold;" id="nomor_antrian_sidik_jari"> - </span></h2>
                                @endif
                            </div>
                        </div>
                        <div class="mt-widget-1 bg-red-intense bg-font-red-intense">
                            <div>&nbsp;</div>
                            <div><h4>Pelayanan SIM BARU</h4></div>
                            <div class="mt-body">
                                @if ($sim_baru)
                                <h2><span style="font-weight:bold;" id="nomor_antrian_sim_baru">{{$sim_baru->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sim_baru->nomor_antrian)}}</span></h2>
                                @else
                                <h2><span style="font-weight:bold;" id="nomor_antrian_sim_baru"> - </span></h2>
                                @endif
                            </div>
                        </div>
                        <div class="mt-widget-1 bg-yellow-gold bg-font-yellow-gold">
                            <div>&nbsp;</div>
                            <div><h4>Pelayanan SIM Perpanjang</h4></div>
                            <div class="mt-body">
                                @if ($sim_perpanjang)
                                <h2><span style="font-weight:bold;" id="nomor_antrian_sim_perpanjang">{{$sim_perpanjang->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sim_perpanjang->nomor_antrian)}}</span></h2>
                                @else
                                <h2><span style="font-weight:bold;" id="nomor_antrian_sim_perpanjang"> - </span></h2>
                                @endif
                            </div>
                        </div>
                        <div class="mt-widget-1 bg-grey-salsa bg-font-grey-salsa">
                            <div>&nbsp;</div>
                            <div><h4>Pelayanan SKCK BARU</h4></div>
                            <div class="mt-body">
                                @if ($skck_baru)
                                <h2><span style="font-weight:bold;" id="nomor_antrian_skck_baru">{{$skck_baru->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($skck_baru->nomor_antrian)}}</span></h2>
                                @else
                                <h2><span style="font-weight:bold;" id="nomor_antrian_skck_baru"> - </span></h2>
                                @endif
                            </div>
                        </div>
                        <div class="mt-widget-1 bg-green-sharp bg-font-green-sharp">
                            <div>&nbsp;</div>
                            <div><h4>Pelayanan SKCK Perpanjang</h4></div>
                            <div class="mt-body">
                                @if ($skck_perpanjang)
                                <h2><span style="font-weight:bold;" id="nomor_antrian_skck_perpanjang">{{$skck_perpanjang->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($skck_perpanjang->nomor_antrian)}}</span></h2>
                                @else
                                <h2><span style="font-weight:bold;" id="nomor_antrian_skck_perpanjang"> - </span></h2>
                                @endif
                            </div>
                        </div>
                        <!--end: widget 1-2 -->
                    </div>
                </div>
                    
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- BEGIN FOOTER -->
                <p class="copyright" style="background-color: #ffffff; padding: 0px 0px;color: #000000;font-size: 2em;margin: 5px 0px;">
                <marquee>
                    {{$footnote}}
                </marquee>
                </p>

                <!-- END FOOTER -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN QUICK SIDEBAR -->

        

        <!-- BEGIN CORE PLUGINS -->
        <script src="public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="public/assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="public/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="public/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="public/assets/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
        <script src="public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="public/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

        <!-- END THEME LAYOUT SCRIPTS -->
        <script type="text/javascript">

            $(document).ready(function() {
                var result = $(".page-content").css('min-height');
                var res = parseInt((parseInt(result))/5);
                console.log(parseInt(result));
                $("#vh").css('height',(parseInt(result)-5));
                $(".mt-widget-1").css('height',res);
            });
            

             window.onload = function() { jam(); }

             function jam() {
              var e = document.getElementById('jam'),
              d = new Date(), h, m, s;
              h = d.getHours();
              m = set(d.getMinutes());
              s = set(d.getSeconds());

              e.innerHTML = h +':'+ m +':'+ s;

              setTimeout('jam()', 1000);

              $.ajax({
                  url: "{{ url('reload-monitor-antrian') }}/1",
                  type: "GET",
                  dataType: "JSON",
                  async : false,
                  success: function(result) {
                    $('#nomor_antrian_sim_baru').text(result.kode_antrian);
                  }
              })

              $.ajax({
                  url: "{{ url('reload-monitor-antrian') }}/2",
                  type: "GET",
                  dataType: "JSON",
                  async : false,
                  success: function(result) {
                    $('#nomor_antrian_sim_perpanjang').text(result.kode_antrian);
                  }
              })

              $.ajax({
                  url: "{{ url('reload-monitor-antrian') }}/3",
                  type: "GET",
                  dataType: "JSON",
                  async : false,
                  success: function(result) {
                    $('#nomor_antrian_skck_baru').text(result.kode_antrian);
                  }
              })

              $.ajax({
                  url: "{{ url('reload-monitor-antrian') }}/4",
                  type: "GET",
                  dataType: "JSON",
                  async : false,
                  success: function(result) {
                    $('#nomor_antrian_skck_perpanjang').text(result.kode_antrian);
                  }
              })

              $.ajax({
                  url: "{{ url('reload-monitor-antrian') }}/5",
                  type: "GET",
                  dataType: "JSON",
                  async : false,
                  success: function(result) {
                    $('#nomor_antrian_sidik_jari').text(result.kode_antrian);
                  }
              })
             }

             function set(e) {
              e = e < 10 ? '0'+ e : e;
              return e;
             }
        </script>

    </body>

</html>