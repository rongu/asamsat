<!doctype html>
<html lang="en" class="deeppurple-theme">

<head>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="Maxartkiller">


    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="{{ asset('fimobile/vendor/materializeicon/material-icons.css') }}">

    <!-- Roboto fonts CSS -->

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('fimobile/vendor/bootstrap-4.4.1/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Swiper CSS -->
    <link href="{{ asset('fimobile/vendor/swiper/css/swiper.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('fimobile/css/style.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" /> 

    <style type="text/css">
        .deeppurple-theme-bg, .deeppurple-theme body, .deeppurple-theme body.sidemenu-open, .deeppurple-theme .bg-template, .deeppurple-theme .loader-screen {
            background: #0095ff ;
            background: -moz-linear-gradient(-45deg, #0095ff  0%, #00189a 100%);
            background: -webkit-gradient(left top, right bottom, color-stop(0%, #0095ff ), color-stop(100%, #00189a));
            background: -webkit-linear-gradient(-45deg, #0095ff  0%, #00189a 100%);
            background: -o-linear-gradient(-45deg, #0095ff  0%, #00189a 100%);
            background: -ms-linear-gradient(-45deg, #0095ff  0%, #00189a 100%);
            background: linear-gradient(135deg, #0095ff 0%, #00189a 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#d800ff', endColorstr='#00189a', GradientType=1);
        }

        .deeppurple-theme .wrapper {
            background: #fcedff;
            background: -moz-linear-gradient(top, #fcedff 60%, #ffffff 100%);
            background: -webkit-gradient(left top, left bottom, color-stop(0%, #fcedff), color-stop(100%, #ffffff));
            background: -webkit-linear-gradient(top, #fcedff 60%, #ffffff 100%);
            background: -o-linear-gradient(top, #fcedff 60%, #ffffff 100%);
            background: -ms-linear-gradient(top, #fcedff 60%, #ffffff 100%);
            background: linear-gradient(to bottom, #efefef 60%, #ffffff 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fcedff', endColorstr='#ffffff', GradientType=0);
            background-attachment: fixed;
            background-position: center top;
            background-size: 100% 100%;
        }

        .btn i:last-child {
            margin-left: 5px;
            margin-right: 5px;
        }

        .deeppurple-theme .text-template, .deeppurple-theme .btn-link, .deeppurple-theme a, .deeppurple-theme .float-label .form-control:focus + .form-control-label, .deeppurple-theme .btn-light:hover, .deeppurple-theme .btn-light:focus, .deeppurple-theme .btn-light:not(:disabled):not(.disabled).active, .deeppurple-theme .btn.btn-link-default:hover, .deeppurple-theme .btn.btn-link-default:focus, .deeppurple-theme .btn.btn-link-default.active {
            color: #2003fc;
        }

        .deeppurple-theme .btn-default, .deeppurple-theme .custom-control-input:checked ~ .custom-control-label::before, .deeppurple-theme .small-slide .swiper-slide .card:hover, .deeppurple-theme .small-slide .swiper-slide .card:focus, .deeppurple-theme .filter, .deeppurple-theme .chosen-container .chosen-results li.highlighted, .deeppurple-theme .page-item.active .page-link, .deeppurple-theme .nav-pills .nav-link.active, .deeppurple-theme .nav-pills .show > .nav-link, .deeppurple-theme .footer .btn i::after {
            background-color: #2003fc;
        }

        .deeppurple-theme .figure-profile, .deeppurple-theme .form-control:focus, .deeppurple-theme .custom-control-input:focus ~ .custom-control-label::before, .deeppurple-theme .custom-control-input:checked ~ .custom-control-label::before, .deeppurple-theme .float-label .form-control:focus, .deeppurple-theme .page-item.active .page-link, .deeppurple-theme .btn-outline-default {
            border-color: #2003fc;
        }
        .circle {
          width: 158px;
          height: 158px;
          line-height: 158px;
          border-radius: 50%;
          font-size: 50px;
          color: #2003fc;
          text-align: center;
          background: #fff
        }
        .circle2 {
          width: 60px;
          height: 60px;
          line-height: 60px;
          border-radius: 50%;
          font-size: 30px;
          color: #2003fc;
          text-align: center;
          background: #fff
        }
    </style>
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
</head>

<body>
    
    <div class="row no-gutters vh-100 loader-screen">
        <div class="col align-self-center text-white text-center">
            <img src="{{ asset('images/splash.png') }}" alt="logo">
            <h4 class="mt-3"><span class="font-weight-light ">Pendaftaran Antrian Samsat Online</h4>
            <h3 class="mt-3"><span class="font-weight-light ">Samsat</span> Bengkulu Utara</h3>
            <div class="laoderhorizontal">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>

    <div class="wrapper homepage">
        <!-- header -->
        <div class="header">
            <div class="row no-gutters">
                <div class="col-auto">
                    &nbsp;
                </div>
                <div class="col text-center"><img src="{{ asset('fimobile/img/logo-header.png') }}" alt="" class="header-logo"></div>
                <div class="col-auto">&nbsp;
                </div>
            </div>
        </div>
        <!-- header ends -->
        @yield('content')
        



    </div>



    <!-- jquery, popper and bootstrap js -->
    <script src="{{ asset('fimobile/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('fimobile/js/popper.min.js') }}"></script>
    <script src="{{ asset('fimobile/vendor/bootstrap-4.4.1/js/bootstrap.min.js') }}"></script>
    <!-- swiper js -->
    <script src="{{ asset('fimobile/vendor/swiper/js/swiper.min.js') }}"></script>

    <!-- cookie js -->
    <script src="{{ asset('fimobile/vendor/cookie/jquery.cookie.js') }}"></script>

    <!-- template custom js -->

    <script src="{{ asset('fimobile/js/main.js') }}"></script>
    @yield('assets-bottom')
</body>

</html>
