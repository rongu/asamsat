@extends('layouts.app-template')

@section('content')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Dashboard</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->

    <!-- begin audio -->

     <div class="audio">
        <audio id="in" src="{{ asset('assets/audio/in.wav') }}"></audio>
        <audio id="out" src="{{ asset('assets/audio/out.wav') }}"></audio>
        <audio id="suarabel" src="{{ asset('assets/audio/Airport_Bell.wav') }}"></audio>
        <audio id="suarabelnomorurut" src="{{ asset('assets/audio/nomor-urut.wav') }}"></audio> 
        <audio id="urut1" src="{{ asset('assets/audio/1.wav') }}"></audio> 
        <audio id="urut2" src="{{ asset('assets/audio/2.wav') }}"></audio> 
        <audio id="urut3" src="{{ asset('assets/audio/3.wav') }}"></audio> 
        <audio id="urut4" src="{{ asset('assets/audio/4.wav') }}"></audio> 
        <audio id="urut5" src="{{ asset('assets/audio/5.wav') }}"></audio> 
        <audio id="urut6" src="{{ asset('assets/audio/6.wav') }}"></audio> 
        <audio id="urut7" src="{{ asset('assets/audio/7.wav') }}"></audio> 
        <audio id="urut8" src="{{ asset('assets/audio/8.wav') }}"></audio> 
        <audio id="urut9" src="{{ asset('assets/audio/9.wav') }}"></audio> 
        <audio id="urut0" src="{{ asset('assets/audio/nol.wav') }}"></audio> 
        <audio id="antrianA" src="{{ asset('assets/audio/A.wav') }}"></audio> 
        <audio id="antrianB" src="{{ asset('assets/audio/B.wav') }}"></audio> 
        <audio id="antrianC" src="{{ asset('assets/audio/C.wav') }}"></audio> 
        <audio id="antrianD" src="{{ asset('assets/audio/D.wav') }}"></audio> 
        <audio id="antrianE" src="{{ asset('assets/audio/E.wav') }}"></audio> 
        <audio id="loket" src="{{ asset('assets/audio/loket.wav') }}"></audio> 
        <audio id="sidikjari" src="{{ asset('assets/audio/sidikjari.wav') }}"></audio> 
        <audio id="simbaru" src="{{ asset('assets/audio/simbaru.wav') }}"></audio> 
        <audio id="simperpanjang" src="{{ asset('assets/audio/simperpanjang.wav') }}"></audio> 
        <audio id="skckbaru" src="{{ asset('assets/audio/skckbaru.wav') }}"></audio> 
        <audio id="skckperpanjang" src="{{ asset('assets/audio/skckperpanjang.wav') }}"></audio> 
    </div>
    <!-- end audio -->

    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bubble font-dark hide"></i>
                        <span class="caption-subject font-hide bold uppercase">Daftar Antrian</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6">
                            <!--begin: widget 1-1 -->
                            <div class="mt-widget-1">
                                <div>&nbsp;</div>
                                <div><h4>{{date('d-M-Y')}}</h4></div>
                                <div class="mt-body">
                                    <center>
                                    <h2 style="font-family: verdana;" id="jam"></h2>
                                    </center>                                    
                                </div>
                            </div>
                            <!--end: widget 1-1 -->
                        </div>         
                        <div class="col-md-6">
                            <!--begin: widget 1-2 -->
                            <div class="mt-widget-1">
                                <div>&nbsp;</div>
                                <div><h4>Pelayanan Perekaman Sidik Jari</h4></div>
                                <div class="mt-body">
                                    @if ($sidik_jari)
                                    <h2><span id="nomor_antrian_sidik_jari">{{$sidik_jari->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sidik_jari->nomor_antrian)}}</span></h2>
                                    <p class="mt-user-title">Total Antrian : <span id="total_antrian_sidik_jari">{{$sidik_jari->total}}</span> </p>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">
                                            <a id="btn-prev-sidik-jari" data-mdid="{{$sidik_jari->md_pelayanan_id}}" data-tanggal="{{$sidik_jari->tanggal_pelayanan}}" data-antrian="{{$sidik_jari->nomor_antrian}}" class="btn font-yellow">
                                                <i class="fa fa-step-backward"></i> </a>
                                            <a id="btn-voice-sidik-jari" data-kode="{{$sidik_jari->kode_antrian}}" data-urut="{{App\Http\Controllers\ApplicationController::getUrut($sidik_jari->nomor_antrian)}}" data-loket="sidikjari" class="btn font-blue">
                                                <i class="fa fa-bullhorn"></i> </a>
                                            <a id="btn-next-sidik-jari" data-mdid="{{$sidik_jari->md_pelayanan_id}}" data-tanggal="{{$sidik_jari->tanggal_pelayanan}}" data-antrian="{{$sidik_jari->nomor_antrian}}" class="btn font-green">
                                                <i class="fa fa-step-forward"></i> </a>
                                        </div>
                                    </div>
                                    @else
                                    <h2>&nbsp; - &nbsp;</h2>
                                    <p class="mt-user-title">Total Antrian : 0 </p>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">
                                            <a class="btn font-yellow">
                                                <i class="fa fa-step-backward"></i> </a>
                                            <a class="btn font-blue">
                                                <i class="fa fa-bullhorn"></i> </a>    
                                            <a class="btn font-green">
                                                <i class="fa fa-step-forward"></i> </a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <!--end: widget 1-2 -->
                        </div>
                        <div class="col-md-12"><br></div>
                        <div class="col-md-6">
                            <!--begin: widget 1-3 -->
                            <div class="mt-widget-1">
                                <div>&nbsp;</div>
                                <div><h4>Pelayanan SIM BARU</h4></div>
                                <div class="mt-body">
                                    @if ($sim_baru)
                                    <h2><span id="nomor_antrian_sim_baru">{{$sim_baru->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sim_baru->nomor_antrian)}}</span></h2>
                                    <p class="mt-user-title">Total Antrian : <span id="total_antrian_sim_baru">{{$sim_baru->total}}</span> </p>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">
                                            <a id="btn-prev-sim-baru" data-mdid="{{$sim_baru->md_pelayanan_id}}" data-tanggal="{{$sim_baru->tanggal_pelayanan}}" data-antrian="{{$sim_baru->nomor_antrian}}" class="btn font-yellow">
                                                <i class="fa fa-step-backward"></i> </a>
                                            <a id="btn-voice-sim-baru" data-kode="{{$sim_baru->kode_antrian}}" data-urut="{{App\Http\Controllers\ApplicationController::getUrut($sim_baru->nomor_antrian)}}" data-loket="simbaru" class="btn font-blue">
                                                <i class="fa fa-bullhorn"></i> </a>    
                                            <a id="btn-next-sim-baru" data-mdid="{{$sim_baru->md_pelayanan_id}}" data-tanggal="{{$sim_baru->tanggal_pelayanan}}" data-antrian="{{$sim_baru->nomor_antrian}}" class="btn font-green">
                                                <i class="fa fa-step-forward"></i> </a>
                                        </div>
                                    </div>
                                    @else
                                    <h2>&nbsp; - &nbsp;</h2>
                                    <p class="mt-user-title">Total Antrian : 0 </p>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">
                                            <a class="btn font-yellow">
                                                <i class="fa fa-step-backward"></i> </a>
                                            <a class="btn font-blue">
                                                <i class="fa fa-bullhorn"></i> </a>     
                                            <a class="btn font-green">
                                                <i class="fa fa-step-forward"></i> </a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <!--end: widget 1-3 -->
                        </div>
                        
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                            <!--begin: widget 1-3 -->
                            <div class="mt-widget-1">
                                <div>&nbsp;</div>
                                <div><h4>Pelayanan SIM Perpanjang</h4></div>
                                <div class="mt-body">
                                    @if ($sim_perpanjang)
                                    <h2><span id="nomor_antrian_sim_perpanjang">{{$sim_perpanjang->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sim_perpanjang->nomor_antrian)}}</span></h2>
                                    <p class="mt-user-title">Total Antrian : <span id="total_antrian_sim_perpanjang">{{$sim_perpanjang->total}}</span> </p>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">
                                            <a id="btn-prev-sim-perpanjang" data-mdid="{{$sim_perpanjang->md_pelayanan_id}}" data-tanggal="{{$sim_perpanjang->tanggal_pelayanan}}" data-antrian="{{$sim_perpanjang->nomor_antrian}}" class="btn font-yellow">
                                                <i class="fa fa-step-backward"></i> </a>
                                            <a id="btn-voice-sim-perpanjang" data-kode="{{$sim_perpanjang->kode_antrian}}" data-urut="{{App\Http\Controllers\ApplicationController::getUrut($sim_perpanjang->nomor_antrian)}}" data-loket="simperpanjang" class="btn font-blue">
                                                <i class="fa fa-bullhorn"></i> </a>      
                                            <a id="btn-next-sim-perpanjang" data-mdid="{{$sim_perpanjang->md_pelayanan_id}}" data-tanggal="{{$sim_perpanjang->tanggal_pelayanan}}" data-antrian="{{$sim_perpanjang->nomor_antrian}}" class="btn font-green">
                                                <i class="fa fa-step-forward"></i> </a>
                                        </div>
                                    </div>
                                    @else
                                    <h2>&nbsp; - &nbsp;</h2>
                                    <p class="mt-user-title">Total Antrian : 0 </p>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">
                                            <a class="btn font-yellow">
                                                <i class="fa fa-step-backward"></i> </a>
                                            <a class="btn font-blue">
                                                <i class="fa fa-bullhorn"></i> </a>     
                                            <a class="btn font-green">
                                                <i class="fa fa-step-forward"></i> </a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                            <!--end: widget 1-3 -->
                        </div>
                        <div class="col-md-12"><br></div>
                        <div class="col-md-6">
                            <!--begin: widget 1-3 -->
                            <div class="mt-widget-1">
                                <div>&nbsp;</div>
                                <div><h4>Pelayanan SKCK BARU</h4></div>
                                <div class="mt-body">
                                    @if ($skck_baru)
                                    <h2><span id="nomor_antrian_skck_baru">{{$skck_baru->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($skck_baru->nomor_antrian)}}</span></h2>
                                    <p class="mt-user-title">Total Antrian : <span id="total_antrian_skck_baru">{{$skck_baru->total}}</span> </p>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">
                                            <a id="btn-prev-skck-baru" data-mdid="{{$skck_baru->md_pelayanan_id}}" data-tanggal="{{$skck_baru->tanggal_pelayanan}}" data-antrian="{{$skck_baru->nomor_antrian}}" class="btn font-yellow">
                                                <i class="fa fa-step-backward"></i> </a>
                                            <a id="btn-voice-skck-baru" data-kode="{{$skck_baru->kode_antrian}}" data-urut="{{App\Http\Controllers\ApplicationController::getUrut($skck_baru->nomor_antrian)}}" data-loket="skckbaru" class="btn font-blue">
                                                <i class="fa fa-bullhorn"></i> </a>      
                                            <a id="btn-next-skck-baru" data-mdid="{{$skck_baru->md_pelayanan_id}}" data-tanggal="{{$skck_baru->tanggal_pelayanan}}" data-antrian="{{$skck_baru->nomor_antrian}}" class="btn font-green">
                                                <i class="fa fa-step-forward"></i> </a>
                                        </div>
                                    </div>
                                    @else
                                    <h2>&nbsp; - &nbsp;</h2>
                                    <p class="mt-user-title">Total Antrian : 0 </p>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">
                                            <a class="btn font-yellow">
                                                <i class="fa fa-step-backward"></i> </a>
                                            <a class="btn font-blue">
                                                <i class="fa fa-bullhorn"></i> </a>     
                                            <a class="btn font-green">
                                                <i class="fa fa-step-forward"></i> </a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <!--end: widget 1-3 -->
                        </div>
                        <div class="col-md-6">
                            <!--begin: widget 1-3 -->
                            <div class="mt-widget-1">
                                <div>&nbsp;</div>
                                <div><h4>Pelayanan SKCK Perpanjang</h4></div>
                                <div class="mt-body">
                                    @if ($skck_perpanjang)
                                    <h2><span id="nomor_antrian_skck_perpanjang">{{$skck_perpanjang->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($skck_perpanjang->nomor_antrian)}}</span></h2>
                                    <p class="mt-user-title">Total Antrian : <span id="total_antrian_skck_perpanjang">{{$skck_perpanjang->total}}</span> </p>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">
                                            <a id="btn-prev-skck-perpanjang" data-mdid="{{$skck_perpanjang->md_pelayanan_id}}" data-tanggal="{{$skck_perpanjang->tanggal_pelayanan}}" data-antrian="{{$skck_perpanjang->nomor_antrian}}" class="btn font-yellow">
                                                <i class="fa fa-step-backward"></i> </a>
                                            <a id="btn-voice-skck-perpanjang" data-kode="{{$skck_perpanjang->kode_antrian}}" data-urut="{{App\Http\Controllers\ApplicationController::getUrut($skck_perpanjang->nomor_antrian)}}" data-loket="skckperpanjang" class="btn font-blue">
                                                <i class="fa fa-bullhorn"></i> </a>     
                                            <a id="btn-next-skck-perpanjang" data-mdid="{{$skck_perpanjang->md_pelayanan_id}}" data-tanggal="{{$skck_perpanjang->tanggal_pelayanan}}" data-antrian="{{$skck_perpanjang->nomor_antrian}}" class="btn font-green">
                                                <i class="fa fa-step-forward"></i> </a>
                                        </div>
                                    </div>
                                    @else
                                    <h2>&nbsp; - &nbsp;</h2>
                                    <p class="mt-user-title">Total Antrian : 0 </p>
                                    <div class="mt-stats">
                                        <div class="btn-group btn-group btn-group-justified">
                                            <a class="btn font-yellow">
                                                <i class="fa fa-step-backward"></i> </a>
                                            <a class="btn font-blue">
                                                <i class="fa fa-bullhorn"></i> </a>     
                                            <a class="btn font-green">
                                                <i class="fa fa-step-forward"></i> </a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <!--end: widget 1-3 -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('assets-bottom')
<script type="text/javascript">
    window.onload = function() { jam(); }

     function jam() {
      var e = document.getElementById('jam'),
      d = new Date(), h, m, s;
      h = d.getHours();
      m = set(d.getMinutes());
      s = set(d.getSeconds());

      e.innerHTML = h +':'+ m +':'+ s;

      setTimeout('jam()', 1000);
     }

     function set(e) {
      e = e < 10 ? '0'+ e : e;
      return e;
     }

    function mulai(antrian,urut, loket){
        var urutan = urut.split("");
        var totalwaktu = 8568.163;
        document.getElementById('in').pause();
        document.getElementById('in').currentTime=0;
        document.getElementById('in').play();
        totalwaktu=document.getElementById('in').duration*1000; 
        setTimeout(function() {
                document.getElementById('suarabelnomorurut').pause();
                document.getElementById('suarabelnomorurut').currentTime=0;
                document.getElementById('suarabelnomorurut').play();
        }, totalwaktu);
        totalwaktu=totalwaktu+1000;
        setTimeout(function() {
                document.getElementById('antrian'+antrian+'').pause();
                document.getElementById('antrian'+antrian+'').currentTime=0;
                document.getElementById('antrian'+antrian+'').play();
            }, totalwaktu);
        totalwaktu=totalwaktu+1000;
        setTimeout(function() {
                document.getElementById('urut'+urutan[0]+'').pause();
                document.getElementById('urut'+urutan[0]+'').currentTime=0;
                document.getElementById('urut'+urutan[0]+'').play();
            }, totalwaktu);
        totalwaktu=totalwaktu+1000;
        setTimeout(function() {
                document.getElementById('urut'+urutan[1]+'').pause();
                document.getElementById('urut'+urutan[1]+'').currentTime=0;
                document.getElementById('urut'+urutan[1]+'').play();
            }, totalwaktu);
        totalwaktu=totalwaktu+1000;
        setTimeout(function() {
                document.getElementById('urut'+urutan[2]+'').pause();
                document.getElementById('urut'+urutan[2]+'').currentTime=0;
                document.getElementById('urut'+urutan[2]+'').play();
            }, totalwaktu);
        totalwaktu=totalwaktu+1000;
        setTimeout(function() {
                document.getElementById('loket').pause();
                document.getElementById('loket').currentTime=0;
                document.getElementById('loket').play();
            }, totalwaktu);
        totalwaktu=totalwaktu+1000;
        setTimeout(function() {
                document.getElementById(''+loket+'').pause();
                document.getElementById(''+loket+'').currentTime=0;
                document.getElementById(''+loket+'').play();
            }, totalwaktu);
        totalwaktu=totalwaktu+1000;
        setTimeout(function() {
                for (var i = 0 ; i < urut.toString().length; i++) {
                    $("#suarabel"+i+"").remove();
                };
            }, totalwaktu);
        totalwaktu=totalwaktu+1000;
        
        setTimeout(function(){
            document.getElementById('out').pause();
            document.getElementById('out').currentTime=0;
            document.getElementById('out').play();
        }, totalwaktu);
        totalwaktu=totalwaktu+1000;
    }

    $('#btn-prev-sidik-jari').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan memundurkan nomor antrian?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('prev-status-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        $('#nomor_antrian_sidik_jari').text(result.kode_antrian);
                        $('#total_antrian_sidik_jari').text(result.total);
                        $('#btn-prev-sidik-jari').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-prev-sidik-jari').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-prev-sidik-jari').attr('data-antrian',result.nomor_antrian);
                        $('#btn-next-sidik-jari').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-next-sidik-jari').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-next-sidik-jari').attr('data-antrian',result.nomor_antrian);
                      }
                })
                swal("Berhasil", "Antrian Sudah dimundurkan.", "success");    
            } else {
                swal("Batal", "Anda membatalkan proses mundur antrian.", "error");
            }
        });        
    });

    $('#btn-prev-sim-baru').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan memundurkan nomor antrian?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('prev-status-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        $('#nomor_antrian_sim_baru').text(result.kode_antrian);
                        $('#total_antrian_sim_baru').text(result.total);
                        $('#btn-prev-sim-baru').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-prev-sim-baru').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-prev-sim-baru').attr('data-antrian',result.nomor_antrian);
                        $('#btn-next-sim-baru').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-next-sim-baru').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-next-sim-baru').attr('data-antrian',result.nomor_antrian);
                      }
                })
                swal("Berhasil", "Antrian Sudah dimundurkan.", "success");    
            } else {
                swal("Batal", "Anda membatalkan proses mundur antrian.", "error");
            }
        });        
    });

    $('#btn-prev-sim-perpanjang').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan melanjutkan nomor antrian?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('prev-status-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        $('#nomor_antrian_sim_perpanjang').text(result.kode_antrian);
                        $('#total_antrian_sim_perpanjang').text(result.total);
                        $('#btn-prev-sim-perpanjang').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-prev-sim-perpanjang').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-prev-sim-perpanjang').attr('data-antrian',result.nomor_antrian);
                        $('#btn-next-sim-perpanjang').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-next-sim-perpanjang').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-next-sim-perpanjang').attr('data-antrian',result.nomor_antrian);
                      }
                })
                swal("Berhasil", "Antrian Sudah dimundurkan.", "success");    
            } else {
                swal("Batal", "Anda membatalkan proses mundur antrian.", "error");
            }
        });        
    });

    $('#btn-prev-skck-baru').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan melanjutkan nomor antrian?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('prev-status-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        $('#nomor_antrian_skck_baru').text(result.kode_antrian);
                        $('#total_antrian_skck_baru').text(result.total);
                        $('#btn-prev-skck-baru').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-prev-skck-baru').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-prev-skck-baru').attr('data-antrian',result.nomor_antrian);
                        $('#btn-next-skck-baru').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-next-skck-baru').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-next-skck-baru').attr('data-antrian',result.nomor_antrian);
                      }
                })
                swal("Berhasil", "Antrian Sudah dimundurkan.", "success");    
            } else {
                swal("Batal", "Anda membatalkan proses mundur antrian.", "error");
            }
        });        
    });

    $('#btn-prev-skck-perpanjang').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan melanjutkan nomor antrian?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('prev-status-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        $('#nomor_antrian_skck_perpanjang').text(result.kode_antrian);
                        $('#total_antrian_skck_perpanjang').text(result.total);
                        $('#btn-prev-skck_perpanjang').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-prev-skck_perpanjang').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-prev-skck_perpanjang').attr('data-antrian',result.nomor_antrian);
                        $('#btn-next-skck_perpanjang').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-next-skck_perpanjang').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-next-skck_perpanjang').attr('data-antrian',result.nomor_antrian);
                      }
                })
                swal("Berhasil", "Antrian Sudah dimundurkan.", "success");
            } else {
                swal("Batal", "Anda membatalkan proses mundur antrian.", "error");
            }
        });
    });    

    $('#btn-voice-sidik-jari').on('click',function(e){
        var kode = e.currentTarget.dataset.kode;
        var urut = e.currentTarget.dataset.urut;
        var loket = e.currentTarget.dataset.loket;
        
        mulai(kode,urut,loket);
        //say('code queue number '+kode+' '+urut+' please go to the counter'+loket);
    });

    $('#btn-voice-sim-baru').on('click',function(e){
        var kode = e.currentTarget.dataset.kode;
        var urut = e.currentTarget.dataset.urut;
        var loket = e.currentTarget.dataset.loket;
        
        mulai(kode,urut,loket);
        //say('code queue number '+kode+' '+urut+' please go to the counter'+loket);
    });    

    $('#btn-voice-sim-perpanjang').on('click',function(e){
        var kode = e.currentTarget.dataset.kode;
        var urut = e.currentTarget.dataset.urut;
        var loket = e.currentTarget.dataset.loket;
        
        mulai(kode,urut,loket);
        //say('code queue number '+kode+' '+urut+' please go to the counter'+loket);
    });    

    $('#btn-voice-skck-baru').on('click',function(e){
        var kode = e.currentTarget.dataset.kode;
        var urut = e.currentTarget.dataset.urut;
        var loket = e.currentTarget.dataset.loket;
        
        mulai(kode,urut,loket);
        //say('code queue number '+kode+' '+urut+' please go to the counter'+loket);
    });    

    $('#btn-voice-skck-perpanjang').on('click',function(e){
        var kode = e.currentTarget.dataset.kode;
        var urut = e.currentTarget.dataset.urut;
        var loket = e.currentTarget.dataset.loket;
        mulai(kode,urut,loket);
        //say('code queue number '+kode+' '+urut+' please go to the counter'+loket);
    });    

    function say(m) {
        var msg = new SpeechSynthesisUtterance();
        var voices = window.speechSynthesis.getVoices();
        msg.voice = voices[10];
        msg.voiceURI = "native";
        msg.volume = 1;
        msg.rate = 1;
        msg.pitch = 0.8;
        msg.text = m;
        msg.lang = 'en-GB';
        speechSynthesis.speak(msg);
    };


    $('#btn-next-sidik-jari').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan melanjutkan nomor antrian?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('update-status-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        window.open(result.url,'_blank ');
                        
                        $('#nomor_antrian_sidik_jari').text(result.user.kode_antrian);
                        $('#total_antrian_sidik_jari').text(result.user.total);
                        $('#btn-prev-sidik-jari').attr('data-mdid',result.user.md_pelayanan_id);
                        $('#btn-prev-sidik-jari').attr('data-tanggal',result.user.tanggal_pelayanan);
                        $('#btn-prev-sidik-jari').attr('data-antrian',result.user.nomor_antrian);
                        $('#btn-next-sidik-jari').attr('data-mdid',result.user.md_pelayanan_id);
                        $('#btn-next-sidik-jari').attr('data-tanggal',result.user.tanggal_pelayanan);
                        $('#btn-next-sidik-jari').attr('data-antrian',result.user.nomor_antrian);
                      }
                })
                swal("Berhasil", "Antrian Sudah dilanjut.", "success");    
            } else {
                swal("Batal", "Anda membatalkan proses lanjut antrian.", "error");
            }
        });        
    });

    $('#btn-next-sim-baru').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan melanjutkan nomor antrian?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('update-status-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        window.open(result.url,'_blank ');

                        $('#nomor_antrian_sim_baru').text(result.user.kode_antrian);
                        $('#total_antrian_sim_baru').text(result.user.total);
                        $('#btn-prev-sim-baru').attr('data-mdid',result.user.md_pelayanan_id);
                        $('#btn-prev-sim-baru').attr('data-tanggal',result.user.tanggal_pelayanan);
                        $('#btn-prev-sim-baru').attr('data-antrian',result.user.nomor_antrian);
                        $('#btn-next-sim-baru').attr('data-mdid',result.user.md_pelayanan_id);
                        $('#btn-next-sim-baru').attr('data-tanggal',result.user.tanggal_pelayanan);
                        $('#btn-next-sim-baru').attr('data-antrian',result.user.nomor_antrian);
                      }
                })
                swal("Berhasil", "Antrian Sudah dilanjut.", "success");    
            } else {
                swal("Batal", "Anda membatalkan proses lanjut antrian.", "error");
            }
        });        
    });

    $('#btn-next-sim-perpanjang').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan melanjutkan nomor antrian?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('update-status-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        window.open(result.url,'_blank ');

                        $('#nomor_antrian_sim_perpanjang').text(result.user.kode_antrian);
                        $('#total_antrian_sim_perpanjang').text(result.user.total);
                        $('#btn-prev-sim-perpanjang').attr('data-mdid',result.user.md_pelayanan_id);
                        $('#btn-prev-sim-perpanjang').attr('data-tanggal',result.user.tanggal_pelayanan);
                        $('#btn-prev-sim-perpanjang').attr('data-antrian',result.user.nomor_antrian);
                        $('#btn-next-sim-perpanjang').attr('data-mdid',result.user.md_pelayanan_id);
                        $('#btn-next-sim-perpanjang').attr('data-tanggal',result.user.tanggal_pelayanan);
                        $('#btn-next-sim-perpanjang').attr('data-antrian',result.user.nomor_antrian);
                      }
                })
                swal("Berhasil", "Antrian Sudah dilanjut.", "success");    
            } else {
                swal("Batal", "Anda membatalkan proses lanjut antrian.", "error");
            }
        });        
    });

    $('#btn-next-skck-baru').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan melanjutkan nomor antrian?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('update-status-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        window.open(result.url,'_blank ');

                        $('#nomor_antrian_skck_baru').text(result.user.kode_antrian);
                        $('#total_antrian_skck_baru').text(result.user.total);
                        $('#btn-prev-skck-baru').attr('data-mdid',result.user.md_pelayanan_id);
                        $('#btn-prev-skck-baru').attr('data-tanggal',result.user.tanggal_pelayanan);
                        $('#btn-prev-skck-baru').attr('data-antrian',result.user.nomor_antrian);
                        $('#btn-next-skck-baru').attr('data-mdid',result.user.md_pelayanan_id);
                        $('#btn-next-skck-baru').attr('data-tanggal',result.user.tanggal_pelayanan);
                        $('#btn-next-skck-baru').attr('data-antrian',result.user.nomor_antrian);
                      }
                })
                swal("Berhasil", "Antrian Sudah dilanjut.", "success");    
            } else {
                swal("Batal", "Anda membatalkan proses lanjut antrian.", "error");
            }
        });        
    });

    $('#btn-next-skck-perpanjang').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan melanjutkan nomor antrian?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('update-status-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        window.open(result.url,'_blank ');

                        $('#nomor_antrian_skck_perpanjang').text(result.user.kode_antrian);
                        $('#total_antrian_skck_perpanjang').text(result.user.total);
                        $('#btn-prev-skck_perpanjang').attr('data-mdid',result.user.md_pelayanan_id);
                        $('#btn-prev-skck_perpanjang').attr('data-tanggal',result.user.tanggal_pelayanan);
                        $('#btn-prev-skck_perpanjang').attr('data-antrian',result.user.nomor_antrian);
                        $('#btn-next-skck_perpanjang').attr('data-mdid',result.user.md_pelayanan_id);
                        $('#btn-next-skck_perpanjang').attr('data-tanggal',result.user.tanggal_pelayanan);
                        $('#btn-next-skck_perpanjang').attr('data-antrian',result.user.nomor_antrian);
                      }
                })
                swal("Berhasil", "Antrian Sudah dilanjut.", "success");
            } else {
                swal("Batal", "Anda membatalkan proses lanjut antrian.", "error");
            }
        });

        
    });
</script>

@endsection    