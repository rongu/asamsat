@extends('layouts.application')

@section('content')


<div class="container">
            <div class="card bg-template shadow mt-4 h-50">
                <div class="card-body">
                    <div class="row">
                        <div class="col pl-0 align-self-center">
                             <a href="{{route('go.apps')}}"><i class="material-icons float-left">&nbsp;&nbsp;&nbsp;arrow_back</i></a>
                            <center><h5 class="mb-1">Pengaturan</h5></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row text-center mt-4">
                <div class="col-12 col-md-12">
                    <div class="row">
                        <div class="col">
                            <div id="accordion2">
                                <div class="card mb-1 border-0 rounded-0">
                                    <div class="card-header bg-primary rounded-0 py-2" id="headingOne2">
                                        <a href="" class=" text-white" data-toggle="collapse" data-target="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">
                                            Tentang Pendaftaran Online Samsat Bengkulu Utara
                                        </a>
                                    </div>

                                    <div id="collapseOne2" class="collapse show" data-parent="#accordion2">
                                        <div class="card-body">
                                            Aplikasi Web Pendaftaran Online Samsat Bengkulu Utara
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-1 border-0 rounded-0">
                                    <div class="card-header bg-primary rounded-0 py-2" id="headingTwo2">
                                        <a href="" class=" text-white collapsed" data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                                            FAQ
                                        </a>
                                    </div>
                                    <div id="collapseTwo2" class="collapse" data-parent="#accordion2">
                                        <div class="card-body">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-1 border-0 rounded-0">
                                    <div class="card-header bg-primary rounded-0 py-2" id="headingThree2">
                                        <a href="" class=" text-white collapsed" data-toggle="collapse" data-target="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2">
                                            Hubungi Kami
                                        </a>
                                    </div>
                                    <div id="collapseThree2" class="collapse" data-parent="#accordion2">
                                        <div class="card-body">
                                            Alamat Kami :<br> 
                                            Kontak Kami :
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- footer-->
        <div class="footer">
            <div class="no-gutters">
                <div class="col-auto mx-auto">
                    <div class="row no-gutters justify-content-center">
                        <div class="col-auto">
                            <a href="{{route('logout')}}" class="btn btn-link-default active">
                                <i class="material-icons">power_settings_new</i>
                            </a>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        <!-- footer ends-->

@endsection