@extends('layouts.application')

@section('content')

<div class="container">
    <div class="card bg-template shadow mt-4 h-50">
        <div class="card-body">
            <div class="row">
                <div class="col pl-0 align-self-center">
                    <center><h5 class="mb-1">Ambil Antrian</h5></center>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row text-center mt-4">
        <div id="btn_sim_baru" class="col-12 col-md-12 btn">
            <div class="card shadow border-0 mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col align-self-center">
                            <h4>Pelayanan SIM Baru</h4>
                            @if ($sim_baru)
                            <h5>Nomor Urut : {{$sim_baru->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sim_baru->nomor_antrian)}}</h5>
                            Total Antrian : {{$sim_baru->total}}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="btn_sim_perpanjang" class="col-12 col-md-12 btn">
            <div class="card shadow border-0 mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col align-self-center">
                            <h4>Pelayanan SIM Perpanjangan</h4>
                            @if ($sim_perpanjang)
                            <h5>Nomor Urut : {{$sim_perpanjang->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sim_perpanjang->nomor_antrian)}}</h5>
                            Total Antrian : {{$sim_perpanjang->total}}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="btn_skck_baru" class="col-12 col-md-12 btn">
            <div class="card shadow border-0 mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col align-self-center">
                            <h4>Pelayanan SKCK Baru</h4>
                            @if ($skck_baru)
                            <h5>Nomor Urut : {{$skck_baru->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($skck_baru->nomor_antrian)}}</h5>
                            Total Antrian : {{$skck_baru->total}}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="btn_skck_perpanjang" class="col-12 col-md-12 btn">
            <div class="card shadow border-0 mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col align-self-center">
                            <h4>Pelayanan SKCK Perpanjangan</h4>
                            @if ($skck_perpanjang)
                            <h5>Nomor Urut : {{$skck_perpanjang->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($skck_perpanjang->nomor_antrian)}}</h5>
                            Total Antrian : {{$skck_perpanjang->total}}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if ($sidik_jari)
        <div id="btn_sidik_jari" class="col-12 col-md-12 btn" data-mdid="{{$sidik_jari->md_pelayanan_id}}" data-tanggal="{{$sidik_jari->tanggal_pelayanan}}" data-antrian="{{$sidik_jari->nomor_antrian}}">
            <div class="card shadow border-0 mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col align-self-center">
                            <h4>Perekaman Sidik Jari</h4>
                            <h5>Nomor Urut : {{$sidik_jari->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sidik_jari->nomor_antrian)}}</h5>
                            Total Antrian : {{$sidik_jari->total}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>

@endsection
@section('assets-bottom')
<script type="text/javascript">

    $('#btn_sidik_jari').on('click',function(e){
        var mdid = e.currentTarget.dataset.mdid;
        var tanggal = e.currentTarget.dataset.tanggal;
        var antrian = e.currentTarget.dataset.antrian;

        swal({
          title: "Apakah akan mencetak nomor antrian pelayanan sidik jari?",
          text: "",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Lanjut",
          cancelButtonText: "Tidak, Batal",
        },function(isConfirm){
            if (isConfirm){
                $.ajax({
                      url: "{{ url('update-status-antrian') }}/"+mdid+"/"+tanggal+"/"+antrian,
                      type: "GET",
                      dataType: "JSON",
                      async : false,
                      success: function(result) {
                        $('#nomor_antrian_skck_perpanjang').text(result.kode_antrian);
                        $('#total_antrian_skck_perpanjang').text(result.total);
                        $('#btn-prev-skck_perpanjang').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-prev-skck_perpanjang').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-prev-skck_perpanjang').attr('data-antrian',result.nomor_antrian);
                        $('#btn-next-skck_perpanjang').attr('data-mdid',result.md_pelayanan_id);
                        $('#btn-next-skck_perpanjang').attr('data-tanggal',result.tanggal_pelayanan);
                        $('#btn-next-skck_perpanjang').attr('data-antrian',result.nomor_antrian);
                      }
                })
                swal("Berhasil", "Antrian Sudah dilanjut.", "success");
            } else {
                swal("Batal", "Anda membatalkan proses lanjut antrian.", "error");
            }
        });

        
    });
</script>

@endsection 