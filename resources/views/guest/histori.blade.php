@extends('layouts.application')
@php use Illuminate\Support\Carbon; @endphp
@section('content')


<div class="container">
            <div class="card bg-template shadow mt-4 h-50">
                <div class="card-body">
                    <div class="row">
                        <div class="col pl-0 align-self-center">
                             <a href="{{route('go.apps')}}"><i class="material-icons float-left">&nbsp;&nbsp;&nbsp;arrow_back</i></a>
                            <center><h5 class="mb-1">Reg. History</h5></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row text-center mt-4">
                <div class="col-12 col-md-12">
                    <div class="card shadow border-0 mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 px-0">
                                    <ul class="list-group list-group-flush border-top border-bottom">
                                        @foreach($rehHistory as $val)
                                            <li class="list-group-item">
                                                <div class="row align-items-center">
                                                    <div class="col-auto pr-0">
                                                        <h5 class="font-weight-normal mb-1">{{$val->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($val->nomor_antrian)}}</h5>
                                                    </div>
                                                    <div class="col align-self-center pr-0">
                                                        <h6 class="font-weight-normal mb-1">{{$val->nama_pelayanan}}</h6>
                                                        <p class="text-mute small text-secondary">{{Carbon::parse($val->tanggal_pelayanan)->format('d-m-Y')}}</p>
                                                    </div>
                                                    <div class="col-auto">
                                                        @if ($val->status_pelayanan == 'SELESAI')
                                                            <h6 class="text-success">{{$val->status_pelayanan}}</h6>
                                                        @else
                                                            <h6 class="text-warning">{{$val->status_pelayanan}}</h6>
                                                        @endif
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection