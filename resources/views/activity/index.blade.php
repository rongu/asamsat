@extends('layouts.app-template')

@section('assets-top')
<style type="text/css">
    
    .multiselect-selected-text{
        float: left;
    }

    .multiselect>.caret{
        float: right;
    }

    .multiselect-container.dropdown-menu>li>a {
        padding: 0px 30px;
        color: #6f6f6f;
        text-decoration: none;
        display: block;
        clear: both;
        font-weight: 300;
        line-height: 18px;
        white-space: nowrap;
    }

    .mt-element-ribbon {
        position: relative;
        margin-bottom: 0px;
    }

    .mt-element-ribbon .ribbon {
        padding: .2em .3em;
        z-index: 5;
        float: left;
        margin: 7px 0 0 -2px;
        clear: left;
        position: relative;
        text-align: center;
        width: 49px!important;
    }

    .todo-project-list ul li a {
        font-size: 14px!important;
        padding: 8px 10px;
        word-break: break-all;
    }

    .todo-sidebar {
        float: left;
        width: 300px;
        margin-right: 10px;
    }
</style>
@endsection

@section('content')


<!-- BEGIN PAGE HEADER-->
<!-- BEGIN THEME PANEL -->

<!-- END THEME PANEL -->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Project</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Project Dashboard
    <small>statistics, charts, recent events and reports</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS 1-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN TODO SIDEBAR -->
        <div class="todo-ui">
            <div class="todo-sidebar">
                <div class="portlet light " style="padding-left: 0px;padding-right: 0px;">
                    <div class="portlet-title">
                        <div class="caption" data-toggle="collapse" data-target=".todo-project-list-content">
                            <span class="caption-subject font-green-sharp bold uppercase">PROJECTS </span>
                            <span class="caption-helper visible-sm-inline-block visible-xs-inline-block">click to view project list</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn green btn-circle btn-outline btn-sm todo-projects-config" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-settings"></i> &nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a id="project_new_btn" href="javascript:;"> New Project </a>
                                    </li>
                                    <li class="divider"> </li>
                                    @foreach($mine_pro_tag as $vl)


                                    @php
                                        if($vl->status_task=="Start"){
                                            $bl = "badge-default";
                                        }else if($vl->status_task=="Presentation"){
                                            $bl = "badge-info";
                                        }else if($vl->status_task=="Pending"){
                                            $bl = "badge-danger";
                                        }else if($vl->status_task=="Demonstration"){
                                            $bl = "badge-info";
                                        }else if($vl->status_task=="Bidding"){
                                            $bl = "badge-danger";
                                        }else if($vl->status_task=="Delivery"){
                                            $bl = "badge-info";
                                        }else if($vl->status_task=="Completed"){
                                            $bl = "badge-success";
                                        }else if($vl->status_task=="Closed"){
                                            $bl = "badge-danger";
                                        }

                                    @endphp

                                        <li>
                                            <a href="javascript:;"> {{$vl->status_task}}
                                                <span class="badge {{$bl}}"> {{$vl->jmltask}}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                    
                                    <!-- <li>
                                        <a href="javascript:;"> Completed
                                            <span class="badge badge-success"> 12 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"> Overdue
                                            <span class="badge badge-warning"> 9 </span>
                                        </a>
                                    </li> -->
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;"> Archived Projects </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body todo-project-list-content" style="min-height: 250px!important;max-height: 250px!important; overflow-y: scroll;display: block;padding-left: 10px;">
                        <div class="todo-project-list">
                            
                            <ul class="nav nav-stacked">

                                @foreach($pro as $vl)
                                    <li>
                                        <div class="mt-element-ribbon bg-grey-steel">
                                            <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-success ribbon-border-dash-hor uppercase">
                                                <div class="ribbon-sub ribbon-clip"></div> {{$vl->presentase_progress}} % </div>
                                        </div>
                                        <a href="javascript:;" class="info_project_btn" data-id="{{$vl->id}}">
                                            <span class="badge badge-info"> {{App\Http\Controllers\ActivityController::getCountActivity($vl->id)}} </span> {{$vl->clients_name}} </a>
                                    </li>
                                @endforeach

                                <!-- <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-info"> 6 </span> AirAsia Ads </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-success"> 2 </span> HSBC Promo </a>
                                </li>
                                <li class="active">
                                    <a href="javascript:;">
                                        <span class="badge badge-success"> 3 </span> GlobalEx</a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-default"> 14 </span> Empire City </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-info"> 6 </span> AirAsia Ads </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-danger"> 2 </span> Loop Inc Promo </a>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet light " style="padding-left: 0px;padding-right: 0px;">
                    <div class="portlet-title">
                        <div class="caption" data-toggle="collapse" data-target=".todo-project-list-content-tags">
                            <span class="caption-subject font-red bold uppercase">TAGS </span>
                            <span class="caption-helper visible-sm-inline-block visible-xs-inline-block">click to view</span>
                        </div>
                        <!-- <div class="actions">
                            <div class="actions">
                                <a class="btn btn-circle grey-salsa btn-outline btn-sm" href="javascript:;">
                                    <i class="fa fa-plus"></i> Add </a>
                            </div>
                        </div> -->
                    </div>
                    <div class="portlet-body todo-project-list-content todo-project-list-content-tags" style="display: block;">
                        <div class="todo-project-list">
                            <ul class="nav nav-pills nav-stacked">
                                @foreach($pro_tag as $vl)

                                @php
                                    if($vl->status_task=="Start"){
                                        $bl = "bg-green-meadow";
                                    }else if($vl->status_task=="Presentation"){
                                        $bl = "bg-blue-hoki";
                                    }else if($vl->status_task=="Pending"){
                                        $bl = "bg-yellow-mint";
                                    }else if($vl->status_task=="Demonstration"){
                                        $bl = "bg-blue-hoki";
                                    }else if($vl->status_task=="Bidding"){
                                        $bl = "bg-blue-hoki";
                                    }else if($vl->status_task=="Delivery"){
                                        $bl = "bg-blue";
                                    }else if($vl->status_task=="Completed"){
                                        $bl = "bg-blue";
                                    }else if($vl->status_task=="Closed"){
                                        $bl = "bg-red-thunderbird";
                                    }

                                @endphp
                                    <li>
                                        <a href="javascript:;"><span class="badge {{$bl}}"> {{$vl->jmltask}} </span> {{$vl->status_task}} </a>
                                    </li>
                                @endforeach
                                <!-- <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-danger"> 6 </span> Pending </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-info"> 2 </span> Completed </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-success"> 14 </span> In Progress </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-warning"> 6 </span> Closed </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge badge-info"> 2 </span> Delivered </a>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END TODO SIDEBAR -->
            <!-- BEGIN TODO CONTENT -->
            <div class="todo-content" style="height: 800px;">
                <div class="portlet light ">
                    <!-- PROJECT HEAD -->
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-sharp hide"></i>
                            <span class="caption-helper" id="info_clients">-</span> &nbsp;
                            <span class="caption-subject font-green-sharp bold uppercase" id="info_project">-</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> MANAGE
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a id="task_new_btn" href="javascript:;"> New Task </a>
                                    </li>
<!--                                     <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;"> Pending
                                            <span class="badge badge-danger"> 4 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"> Completed
                                            <span class="badge badge-success"> 12 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"> Overdue
                                            <span class="badge badge-warning"> 9 </span>
                                        </a>
                                    </li> -->
                                    <!-- <li class="divider"> </li> -->
                                    <!-- <li>
                                        <a href="javascript:;"> Delete Project </a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- end PROJECT HEAD -->
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-7 col-sm-4">
                                <div class="todo-tasklist" id="todo-tasklist-id">

                                    <!-- <div class="todo-tasklist-item todo-tasklist-item-border-green">
                                        <img class="todo-userpic pull-left" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px">
                                        <div class="todo-tasklist-item-title"> Slider Redesign </div>
                                        <div class="todo-tasklist-item-text"> Lorem dolor sit amet ipsum dolor sit consectetuer dolore. </div>
                                        <div class="todo-tasklist-controls pull-left">
                                            <span class="todo-tasklist-date">
                                                <i class="fa fa-calendar"></i> 21 Sep 2014 </span>
                                            <span class="todo-tasklist-badge badge badge-roundless">Urgent</span>
                                        </div>
                                    </div>
                                    
                                    <div class="todo-tasklist-item todo-tasklist-item-border-red">
                                        <img class="todo-userpic pull-left" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px">
                                        <div class="todo-tasklist-item-title"> Homepage Alignments to adjust </div>
                                        <div class="todo-tasklist-item-text"> Lorem ipsum dolor sit amet, consectetuer dolore dolor sit amet. </div>
                                        <div class="todo-tasklist-controls pull-left">
                                            <span class="todo-tasklist-date">
                                                <i class="fa fa-calendar"></i> 14 Sep 2014 </span>
                                            <span class="todo-tasklist-badge badge badge-roundless">Important</span>
                                        </div>
                                    </div>
                                    <div class="todo-tasklist-item todo-tasklist-item-border-green">
                                        <img class="todo-userpic pull-left" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px">
                                        <div class="todo-tasklist-item-title"> Slider Redesign </div>
                                        <div class="todo-tasklist-controls pull-left">
                                            <span class="todo-tasklist-date">
                                                <i class="fa fa-calendar"></i> 10 Feb 2015 </span>
                                            <span class="todo-tasklist-badge badge badge-roundless">Important</span>&nbsp; </div>
                                    </div>
                                    <div class="todo-tasklist-item todo-tasklist-item-border-blue">
                                        <img class="todo-userpic pull-left" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px">
                                        <div class="todo-tasklist-item-title"> Contact Us Map Location changes </div>
                                        <div class="todo-tasklist-item-text"> Lorem ipsum amet, consectetuer dolore dolor sit amet. </div>
                                        <div class="todo-tasklist-controls pull-left">
                                            <span class="todo-tasklist-date">
                                                <i class="fa fa-calendar"></i> 04 Oct 2014 </span>
                                            <span class="todo-tasklist-badge badge badge-roundless">Postponed</span>&nbsp;
                                            <span class="todo-tasklist-badge badge badge-roundless">Test</span>
                                        </div>
                                    </div>
                                    <div class="todo-tasklist-item todo-tasklist-item-border-purple">
                                        <img class="todo-userpic pull-left" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px">
                                        <div class="todo-tasklist-item-title"> Projects list new Forms </div>
                                        <div class="todo-tasklist-item-text"> Lorem ipsum dolor sit amet, consectetuer dolore psum dolor sit. </div>
                                        <div class="todo-tasklist-controls pull-left">
                                            <span class="todo-tasklist-date">
                                                <i class="fa fa-calendar"></i> 19 Dec 2014 </span>
                                        </div>
                                    </div>
                                    <div class="todo-tasklist-item todo-tasklist-item-border-yellow">
                                        <img class="todo-userpic pull-left" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px">
                                        <div class="todo-tasklist-item-title"> New Search Keywords </div>
                                        <div class="todo-tasklist-item-text"> Lorem ipsum dolor sit amet, consectetuer sit amet ipsum dolor, consectetuer ipsum consectetuer sit amet dolore. </div>
                                        <div class="todo-tasklist-controls pull-left">
                                            <span class="todo-tasklist-date">
                                                <i class="fa fa-calendar"></i> 02 Feb 2015 </span>
                                            <span class="todo-tasklist-badge badge badge-roundless">Postponed</span>&nbsp; </div>
                                    </div>
                                    <div class="todo-tasklist-item todo-tasklist-item-border-green">
                                        <img class="todo-userpic pull-left" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px">
                                        <div class="todo-tasklist-item-title"> Slider Redesign </div>
                                        <div class="todo-tasklist-controls pull-left">
                                            <span class="todo-tasklist-date">
                                                <i class="fa fa-calendar"></i> 10 Feb 2015 </span>
                                            <span class="todo-tasklist-badge badge badge-roundless">Important</span>&nbsp; </div>
                                    </div>
                                    <div class="todo-tasklist-item todo-tasklist-item-border-red">
                                        <img class="todo-userpic pull-left" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px">
                                        <div class="todo-tasklist-item-title"> Homepage Alignments to adjust </div>
                                        <div class="todo-tasklist-item-text"> Lorem ipsum dolor sit amet, consectetuer dolore psum dolor sit. </div>
                                        <div class="todo-tasklist-controls pull-left">
                                            <span class="todo-tasklist-date">
                                                <i class="fa fa-calendar"></i> 14 Sep 2014 </span>
                                            <span class="todo-tasklist-badge badge badge-roundless">Important</span>
                                        </div>
                                    </div>
                                    <div class="todo-tasklist-item todo-tasklist-item-border-blue">
                                        <img class="todo-userpic pull-left" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px">
                                        <div class="todo-tasklist-item-title"> Contact Us Improvement </div>
                                        <div class="todo-tasklist-item-text"> Lorem ipsum amet, psum dolor sit consectetuer dolore. </div>
                                        <div class="todo-tasklist-controls pull-left">
                                            <span class="todo-tasklist-date">
                                                <i class="fa fa-calendar"></i> 21 Feb 2015 </span>
                                            <span class="todo-tasklist-badge badge badge-roundless">Postponed</span>&nbsp;
                                            <span class="todo-tasklist-badge badge badge-roundless">Primary</span>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="todo-tasklist-devider"> </div>
                            <div class="col-md-5 col-sm-8">
                                    <div id="form_project">
                                        @if(session('success'))
                                        <div class="alert alert-success display-show">
                                        <button class="close" data-close="alert"></button> {{ session('success') }} </div>
                                        @endif

                                        @if(session('error'))
                                        <div class="alert alert-danger display-show">
                                        <button class="close" data-close="alert"></button>  {{ session('error') }} </div>
                                        @endif
                                        <form id="myProject" class="form-horizontal" role="form" action="{{route('activity.project.create')}}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form">
                                            <div class="form-group">
                                                <div class="col-md-8 col-sm-8">
                                                    <div class="todo-taskbody-user">
                                                        
                                                        @if($imgsUsers==null)
                                                            <img class="todo-userpic pull-left" src="{{ asset('images/profile.png') }}" width="50px" height="50px">
                                                        @else
                                                            <img class="todo-userpic pull-left" src="{{ asset('images/profile') }}/{{$imgsUsers}}" width="50px" height="50px">
                                                        @endif

                                                        
                                                        
                                                        <span class="todo-username pull-left" id="project_name_user">{{$nameUser}}</span>
                                                        <!-- <button type="button" class="todo-username-btn btn btn-circle btn-default btn-sm">&nbsp;edit&nbsp;</button> -->
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-4">
                                                    <div class="todo-taskbody-date pull-right">
                                                        <!-- <button type="button" class="todo-username-btn btn btn-circle btn-default btn-sm">&nbsp; Complete &nbsp;</button> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END TASK HEAD -->
                                            <!-- TASK TITLE -->
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <select id="mt_client" name="client_id" class="form-control btn btn-default todo-taskbody-tasktitle" data-width="100%" data-filter="true" data-action-onchange="true" data-label="left" >
                                                        @foreach($client as $key)
                                                            <option value="{{$key->id}}">{{$key->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <input type="text" id="project_name_id" name="project_name" class="form-control todo-taskbody-tasktitle" placeholder="Project Title"> </div>
                                            </div>

                                            <!-- TASK DESC -->
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <textarea id="project_desc_id" name="project_desc" class="form-control todo-taskbody-taskdesc" rows="8" placeholder="Project Description..."></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <select id="mt_employee" name="employee_id[]" multiple="multiple" data-select-all="true" class="form-control btn btn-default todo-taskbody-tasktitle" data-width="100%" data-filter="true"  data-action-onchange="true">
                                                        @foreach($employee as $key)
                                                            <option value="{{$key->users_id}}">{{$key->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- END TASK DESC -->
                                            <!-- TASK DUE DATE -->
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <div class="input-icon">
                                                        <i class="fa fa-calendar"></i>
                                                        <input type="text" id="project_date_id" name="project_date" class="form-control todo-taskbody-due" placeholder="Start Date..."> </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <select id="mt_products" data-height="150" name="products_id[]" multiple="multiple" data-select-all="true" class="form-control btn btn-default todo-taskbody-tasktitle" data-width="100%" data-filter="true"  data-action-onchange="true">
                                                        @foreach($products as $key)
                                                            <option value="{{$key->id}}">{{$key->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- TASK TAGS -->
                                            <!-- <div class="form-group">
                                                <div class="col-md-12">
                                                    <select class="form-control todo-taskbody-tags">
                                                        <option value="Pending">Pending</option>
                                                        <option value="Completed">Completed</option>
                                                        <option value="Testing">Testing</option>
                                                        <option value="Approved">Approed</option>
                                                        <option value="Rejected">Rejected</option>
                                                    </select>
                                                </div>
                                            </div> -->
                                            <!-- TASK TAGS -->
                                            
                                            <div class="form-actions left todo-form-actions">
                                                <button type="submit" class="btn btn-sm green">Start</button>
                                                <button type="reset" class="btn default btn-sm">Reset</button>
                                                <a id="delete_btn" class="btn red-mint btn-outline sbold uppercase btn-sm">Delete</a>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                    
                                    
                                    <div id="form_task">
                                        <!-- TASK HEAD -->
                                        @if(session('success'))
                                        <div class="alert alert-success display-show">
                                        <button class="close" data-close="alert"></button> {{ session('success') }} </div>
                                        @endif

                                        @if(session('error'))
                                        <div class="alert alert-danger display-show">
                                        <button class="close" data-close="alert"></button>  {{ session('error') }} </div>
                                        @endif
                                        <form id="myTask" class="form-horizontal" role="form" action="{{route('activity.task.create')}}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form">
                                            <div class="form-group">
                                                <div class="col-md-8 col-sm-8">
                                                    <div class="todo-taskbody-user">
                                                        
                                                          @if($imgsUsers==null)
                                                            <img class="todo-userpic pull-left" src="{{ asset('images/profile.png') }}" width="50px" height="50px">
                                                        @else
                                                            <img class="todo-userpic pull-left" src="{{ asset('images/profile') }}/{{$imgsUsers}}" width="50px" height="50px">
                                                        @endif


                                                        
                                                        <span class="todo-username pull-left" id="task_name_user">{{$nameUser}}</span>
                                                        <!-- <button type="button" class="todo-username-btn btn btn-circle btn-default btn-sm">&nbsp;edit&nbsp;</button> -->
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-4">
                                                    <div class="todo-taskbody-date pull-right">
                                                        <!-- <button type="button" class="todo-username-btn btn btn-circle btn-default btn-sm">&nbsp; Complete &nbsp;</button> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END TASK HEAD -->
                                            <!-- TASK TITLE -->
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <select id="mt_project" name="project_id" class="form-control btn btn-default todo-taskbody-tasktitle" data-width="100%" data-filter="true" data-action-onchange="true" data-label="left" >
                                                        @foreach($pro as $key)
                                                            <option value="{{$key->id}}">{{$key->clients_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <input type="text" name="task_name" class="form-control todo-taskbody-tasktitle" placeholder="Task Title..."> </div>
                                            </div>
                                            <!-- TASK DESC -->
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <textarea name="task_desc" class="form-control todo-taskbody-taskdesc" rows="8" placeholder="Task Description..."></textarea>
                                                </div>
                                            </div>
                                            <!-- END TASK DESC -->
                                            <!-- TASK DUE DATE -->
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <div class="input-icon">
                                                        <i class="fa fa-calendar"></i>
                                                        <input type="text" name="task_date" class="form-control todo-taskbody-due" placeholder="Due Date..."> </div>
                                                </div>
                                            </div>
                                            <!-- TASK TAGS -->
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <select name="task_status" class="form-control todo-taskbody-tags">
                                                        <option value="Pending">Pending</option>
                                                        <option value="Presentation">Presentation</option>
                                                        <option value="Demonstration">Demonstration</option>
                                                        <option value="Bidding">Bidding</option>                                                        
                                                        <option value="Approved">Approved</option>
                                                        <option value="Delivery">Delivery</option>
                                                        <option value="Completed">Completed</option>
                                                        <option value="Closed">Closed</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- TASK TAGS -->
                                            <div class="form-actions left todo-form-actions">
                                                <button type="submit" class="btn btn-sm green">Add</button>
                                                <button type="reset" class="btn default btn-sm">Reset</button>
                                                <!-- <a id="delete_btn" class="btn red-mint btn-outline sbold uppercase btn-sm">Delete</a> -->
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                    

                                    <!-- <form class="form-horizontal" >
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs ">
                                            <li class="active">
                                                <a href="#tab_1" data-toggle="tab"> Comments </a>
                                            </li>
                                            <li>
                                                <a href="#tab_2" data-toggle="tab"> History </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <ul class="media-list">
                                                            <li class="media">
                                                                <a class="pull-left" href="javascript:;">
                                                                    <img class="todo-userpic" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px"> </a>
                                                                <div class="media-body todo-comment">
                                                                    <button type="button" class="todo-comment-btn btn btn-circle btn-default btn-sm">&nbsp; Reply &nbsp;</button>
                                                                    <p class="todo-comment-head">
                                                                        <span class="todo-comment-username">Christina Aguilera</span> &nbsp;
                                                                        <span class="todo-comment-date">17 Sep 2014 at 2:05pm</span>
                                                                    </p>
                                                                    <p class="todo-text-color"> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra
                                                                        turpis. </p>
                                                                    <div class="media">
                                                                        <a class="pull-left" href="javascript:;">
                                                                            <img class="todo-userpic" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px"> </a>
                                                                        <div class="media-body">
                                                                            <p class="todo-comment-head">
                                                                                <span class="todo-comment-username">Carles Puyol</span> &nbsp;
                                                                                <span class="todo-comment-date">17 Sep 2014 at 4:30pm</span>
                                                                            </p>
                                                                            <p class="todo-text-color"> Thanks so much my dear! </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="media">
                                                                <a class="pull-left" href="javascript:;">
                                                                    <img class="todo-userpic" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px"> </a>
                                                                <div class="media-body todo-comment">
                                                                    <button type="button" class="todo-comment-btn btn btn-circle btn-default btn-sm">&nbsp; Reply &nbsp;</button>
                                                                    <p class="todo-comment-head">
                                                                        <span class="todo-comment-username">Andres Iniesta</span> &nbsp;
                                                                        <span class="todo-comment-date">18 Sep 2014 at 9:22am</span>
                                                                    </p>
                                                                    <p class="todo-text-color"> Cras sit amet nibh libero, in gravida nulla. Scelerisque ante sollicitudin commodo Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum
                                                                        in vulputate at, tempus viverra turpis.
                                                                        <br> </p>
                                                                </div>
                                                            </li>
                                                            <li class="media">
                                                                <a class="pull-left" href="javascript:;">
                                                                    <img class="todo-userpic" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px"> </a>
                                                                <div class="media-body todo-comment">
                                                                    <button type="button" class="todo-comment-btn btn btn-circle btn-default btn-sm">&nbsp; Reply &nbsp;</button>
                                                                    <p class="todo-comment-head">
                                                                        <span class="todo-comment-username">Olivia Wilde</span> &nbsp;
                                                                        <span class="todo-comment-date">18 Sep 2014 at 11:50am</span>
                                                                    </p>
                                                                    <p class="todo-text-color"> Cras sit amet nibh libero, in gravida nulla. Scelerisque ante sollicitudin commodo Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum
                                                                        in vulputate at, tempus viverra turpis.
                                                                        <br> </p>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <ul class="media-list">
                                                            <li class="media">
                                                                <a class="pull-left" href="javascript:;">
                                                                    <img class="todo-userpic" src="{{ asset('assets/pages/media/users/avatar4.jpg') }}" width="27px" height="27px"> </a>
                                                                <div class="media-body">
                                                                    <textarea class="form-control todo-taskbody-taskdesc" rows="4" placeholder="Type comment..."></textarea>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                        <button type="button" class="pull-right btn btn-sm btn-circle green"> &nbsp; Submit &nbsp; </button>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_2">
                                                <ul class="todo-task-history">
                                                    <li>
                                                        <div class="todo-task-history-date"> 20 Jun, 2014 at 11:35am </div>
                                                        <div class="todo-task-history-desc"> Task created </div>
                                                    </li>
                                                    <li>
                                                        <div class="todo-task-history-date"> 21 Jun, 2014 at 10:35pm </div>
                                                        <div class="todo-task-history-desc"> Task category status changed to "Top Priority" </div>
                                                    </li>
                                                    <li>
                                                        <div class="todo-task-history-date"> 22 Jun, 2014 at 11:35am </div>
                                                        <div class="todo-task-history-desc"> Task owner changed to "Nick Larson" </div>
                                                    </li>
                                                    <li>
                                                        <div class="todo-task-history-date"> 30 Jun, 2014 at 8:09am </div>
                                                        <div class="todo-task-history-desc"> Task completed by "Nick Larson" </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </form> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END TODO CONTENT -->
        </div>
    </div>
    <!-- END PAGE CONTENT-->
</div>
</div>
<!-- END CONTENT BODY -->
</div>
@endsection


@section('assets-bottom')
<script>
$(document).ready(function() {


    $("#delete_btn").hide();

    $("#form_task").hide();
    $("#form_project").show();

    $("#project_new_btn").on("click",function(e){
        $("#form_task").hide();
        $("#form_project").show();

        $("#mt_client").multiselect('refresh');
        $("#mt_employee").multiselect('refresh');
        $("#mt_products").multiselect('refresh');
        
        $("#mt_client").multiselect('rebuild');
        $("#mt_employee").multiselect('rebuild');
        $("#mt_products").multiselect('rebuild');


        $('#mt_client').val([]).multiselect('refresh');
        $('#mt_employee').val([]).multiselect('refresh');
        $('#mt_products').val([]).multiselect('refresh');

        $("#project_name_id").val('');
        $("#project_desc_id").val('');
        $("#project_date_id").val('');
    });

    $("#task_new_btn").on("click",function(e){
        $("#form_task").show();
        $("#form_project").hide();
    });
    
    $('#mt_client').each(function(){
        var btn_class = $(this).attr('class');
        var clickable_groups = ($(this).data('clickable-groups')) ? $(this).data('clickable-groups') : false ;
        var collapse_groups = ($(this).data('collapse-groups')) ? $(this).data('collapse-groups') : false ;
        var drop_right = ($(this).data('drop-right')) ? $(this).data('drop-right') : false ;
        var drop_up = ($(this).data('drop-up')) ? $(this).data('drop-up') : false ;
        var select_all = ($(this).data('select-all')) ? $(this).data('select-all') : false ;
        var width = ($(this).data('width')) ? $(this).data('width') : '' ;
        var height = ($(this).data('height')) ? $(this).data('height') : '' ;
        var filter = ($(this).data('filter')) ? $(this).data('filter') : false ;

        // advanced functions
        var onchange_function = function(option, checked, select) {
            // alert('Changed option ' + $(option).val() + '.');
        }
        var dropdownshow_function = function(event) {
            // alert('Dropdown shown.');
        }
        var dropdownhide_function = function(event) {
            // alert('Dropdown Hidden.');
        }

        // init advanced functions
        var onchange = ($(this).data('action-onchange') == true) ? onchange_function : '';
        var dropdownshow = ($(this).data('action-dropdownshow') == true) ? dropdownshow_function : '';
        var dropdownhide = ($(this).data('action-dropdownhide') == true) ? dropdownhide_function : '';

        // template functions
        // init variables
        var li_template;
        if ($(this).attr('multiple')){
            li_template = '<li class="mt-checkbox-list"><a href="javascript:void(0);"><label class="mt-checkbox"> <span></span></label></a></li>';
        } else {
            li_template = '<li><a href="javascript:void(0);"><label></label></a></li>';
        }

        // init multiselect
        $(this).multiselect({
            enableClickableOptGroups: clickable_groups,
            enableCollapsibleOptGroups: collapse_groups,
            enableCaseInsensitiveFiltering: true,
            disableIfEmpty: true,
            enableFiltering: filter,
            includeSelectAllOption: select_all,
            dropRight: drop_right,
            buttonWidth: width,
            maxHeight: height,
            onChange: onchange,
            onDropdownShow: dropdownshow,
            onDropdownHide: dropdownhide,
            buttonClass: btn_class,

            //optionClass: function(element) { return "mt-checkbox"; },
            //optionLabel: function(element) { console.log(element); return $(element).html() + '<span></span>'; },
            /*templates: {
                li: li_template,
            }*/
        });   
    });    

    $('#mt_employee').each(function(){
        var btn_class = $(this).attr('class');
        var clickable_groups = ($(this).data('clickable-groups')) ? $(this).data('clickable-groups') : false ;
        var collapse_groups = ($(this).data('collapse-groups')) ? $(this).data('collapse-groups') : false ;
        var drop_right = ($(this).data('drop-right')) ? $(this).data('drop-right') : false ;
        var drop_up = ($(this).data('drop-up')) ? $(this).data('drop-up') : false ;
        var select_all = ($(this).data('select-all')) ? $(this).data('select-all') : false ;
        var width = ($(this).data('width')) ? $(this).data('width') : '' ;
        var height = ($(this).data('height')) ? $(this).data('height') : '' ;
        var filter = ($(this).data('filter')) ? $(this).data('filter') : false ;

        // advanced functions
        var onchange_function = function(option, checked, select) {
            // alert('Changed option ' + $(option).val() + '.');
        }
        var dropdownshow_function = function(event) {
            // alert('Dropdown shown.');
        }
        var dropdownhide_function = function(event) {
            // alert('Dropdown Hidden.');
        }

        // init advanced functions
        var onchange = ($(this).data('action-onchange') == true) ? onchange_function : '';
        var dropdownshow = ($(this).data('action-dropdownshow') == true) ? dropdownshow_function : '';
        var dropdownhide = ($(this).data('action-dropdownhide') == true) ? dropdownhide_function : '';

        // template functions
        // init variables
        var li_template;
        if ($(this).attr('multiple')){
            li_template = '<li class="mt-checkbox-list"><a href="javascript:void(0);"><label class="mt-checkbox"> <span></span></label></a></li>';
        } else {
            li_template = '<li><a href="javascript:void(0);"><label></label></a></li>';
        }

        // init multiselect
        $(this).multiselect({
            enableClickableOptGroups: clickable_groups,
            enableCollapsibleOptGroups: collapse_groups,
            enableCaseInsensitiveFiltering: true,
            disableIfEmpty: true,
            enableFiltering: filter,
            includeSelectAllOption: select_all,
            dropRight: drop_right,
            buttonWidth: width,
            maxHeight: height,
            onChange: onchange,
            onDropdownShow: dropdownshow,
            onDropdownHide: dropdownhide,
            buttonClass: btn_class,
            nonSelectedText: "Pilih Employee untuk membuat tim",

            //optionClass: function(element) { return "mt-checkbox"; },
            //optionLabel: function(element) { console.log(element); return $(element).html() + '<span></span>'; },
            /*templates: {
                li: li_template,
            }*/
        });   
    });

    $('#mt_project').each(function(){
        var btn_class = $(this).attr('class');
        var clickable_groups = ($(this).data('clickable-groups')) ? $(this).data('clickable-groups') : false ;
        var collapse_groups = ($(this).data('collapse-groups')) ? $(this).data('collapse-groups') : false ;
        var drop_right = ($(this).data('drop-right')) ? $(this).data('drop-right') : false ;
        var drop_up = ($(this).data('drop-up')) ? $(this).data('drop-up') : false ;
        var select_all = ($(this).data('select-all')) ? $(this).data('select-all') : false ;
        var width = ($(this).data('width')) ? $(this).data('width') : '' ;
        var height = ($(this).data('height')) ? $(this).data('height') : '' ;
        var filter = ($(this).data('filter')) ? $(this).data('filter') : false ;

        // advanced functions
        var onchange_function = function(option, checked, select) {
            // alert('Changed option ' + $(option).val() + '.');
        }
        var dropdownshow_function = function(event) {
            // alert('Dropdown shown.');
        }
        var dropdownhide_function = function(event) {
            // alert('Dropdown Hidden.');
        }

        // init advanced functions
        var onchange = ($(this).data('action-onchange') == true) ? onchange_function : '';
        var dropdownshow = ($(this).data('action-dropdownshow') == true) ? dropdownshow_function : '';
        var dropdownhide = ($(this).data('action-dropdownhide') == true) ? dropdownhide_function : '';

        // template functions
        // init variables
        var li_template;
        if ($(this).attr('multiple')){
            li_template = '<li class="mt-checkbox-list"><a href="javascript:void(0);"><label class="mt-checkbox"> <span></span></label></a></li>';
        } else {
            li_template = '<li><a href="javascript:void(0);"><label></label></a></li>';
        }

        // init multiselect
        $(this).multiselect({
            enableClickableOptGroups: clickable_groups,
            enableCollapsibleOptGroups: collapse_groups,
            enableCaseInsensitiveFiltering: true,
            disableIfEmpty: true,
            enableFiltering: filter,
            includeSelectAllOption: select_all,
            dropRight: drop_right,
            buttonWidth: width,
            maxHeight: height,
            onChange: onchange,
            onDropdownShow: dropdownshow,
            onDropdownHide: dropdownhide,
            buttonClass: btn_class,
            nonSelectedText: "Pilih Project",

            //optionClass: function(element) { return "mt-checkbox"; },
            //optionLabel: function(element) { console.log(element); return $(element).html() + '<span></span>'; },
            /*templates: {
                li: li_template,
            }*/
        });   
    });

    $('#mt_products').each(function(){
        var btn_class = $(this).attr('class');
        var clickable_groups = ($(this).data('clickable-groups')) ? $(this).data('clickable-groups') : false ;
        var collapse_groups = ($(this).data('collapse-groups')) ? $(this).data('collapse-groups') : false ;
        var drop_right = ($(this).data('drop-right')) ? $(this).data('drop-right') : false ;
        var drop_up = ($(this).data('drop-up')) ? $(this).data('drop-up') : false ;
        var select_all = ($(this).data('select-all')) ? $(this).data('select-all') : false ;
        var width = ($(this).data('width')) ? $(this).data('width') : '' ;
        var height = ($(this).data('height')) ? $(this).data('height') : '' ;
        var filter = ($(this).data('filter')) ? $(this).data('filter') : false ;

        // advanced functions
        var onchange_function = function(option, checked, select) {
            // alert('Changed option ' + $(option).val() + '.');
        }
        var dropdownshow_function = function(event) {
            // alert('Dropdown shown.');
        }
        var dropdownhide_function = function(event) {
            // alert('Dropdown Hidden.');
        }

        // init advanced functions
        var onchange = ($(this).data('action-onchange') == true) ? onchange_function : '';
        var dropdownshow = ($(this).data('action-dropdownshow') == true) ? dropdownshow_function : '';
        var dropdownhide = ($(this).data('action-dropdownhide') == true) ? dropdownhide_function : '';

        // template functions
        // init variables
        var li_template;
        if ($(this).attr('multiple')){
            li_template = '<li class="mt-checkbox-list"><a href="javascript:void(0);"><label class="mt-checkbox"> <span></span></label></a></li>';
        } else {
            li_template = '<li><a href="javascript:void(0);"><label></label></a></li>';
        }

        // init multiselect
        $(this).multiselect({
            enableClickableOptGroups: clickable_groups,
            enableCollapsibleOptGroups: collapse_groups,
            enableCaseInsensitiveFiltering: true,
            disableIfEmpty: true,
            enableFiltering: filter,
            includeSelectAllOption: select_all,
            dropRight: drop_right,
            buttonWidth: width,
            maxHeight: height,
            onChange: onchange,
            onDropdownShow: dropdownshow,
            onDropdownHide: dropdownhide,
            buttonClass: btn_class,
            nonSelectedText: "Pilih Product",

            //optionClass: function(element) { return "mt-checkbox"; },
            //optionLabel: function(element) { console.log(element); return $(element).html() + '<span></span>'; },
            /*templates: {
                li: li_template,
            }*/
        });   
    });

    $(".info_project_btn").on("click",function(e){
        var id = e.currentTarget.dataset.id;
        
        $('#mt_client').val([]).multiselect('refresh');
        $('#mt_employee').val([]).multiselect('refresh');
        $('#mt_products').val([]).multiselect('refresh');

        $.ajax({
              url: "{{ url('activity-timeline-page') }}/"+id,
              type: "GET",
              dataType: "JSON",
              async : false,
              success: function(result) {
                

                $("#mt_client").multiselect('refresh');
                $("#mt_employee").multiselect('refresh');        
                $("#mt_products").multiselect('refresh');   

                var tim = JSON.parse(result.project.users_id_tim);
                var produk = JSON.parse(result.project.products_id);

                $("#mt_client").multiselect('select',result.project.clients_id);
                $("#mt_employee").multiselect('select',tim);
                $("#mt_products").multiselect('select',produk);
     
                $("#mt_client").multiselect('rebuild');
                $("#mt_employee").multiselect('rebuild');
                $("#mt_products").multiselect('rebuild');


                // $('#mt_client').val(result.project.clients_id).multiselect('refresh');
                // $('#mt_employee').val(result.project.users_id_tim).multiselect('refresh');
                // $('#mt_products').val([result.project.products_id]).multiselect('refresh');
        
                $("#project_name_id").val(result.project.project_name);
                $("#project_desc_id").val(result.project.project_desc);
                $("#project_date_id").val(result.project.project_start_date);

                $("#info_clients").text(result.project.clients_name+" :");
                $("#info_project").text(result.project.project_name);

                if(result.timeline.length > 0){ 
                    var htmlview = '';
                    for (var i = 0; i < result.timeline.length; i++) {
                        var row = result.timeline[i];
                        htmlview += '<div class="todo-tasklist-item '+row.colorStatus+'">';
                            htmlview += '<img class="todo-userpic pull-left" src="'+row.imgs+'" width="27px" height="27px">';
                            htmlview += '<div class="todo-tasklist-item-title"> '+row.task_name+' </div>';
                            htmlview += '<div class="todo-tasklist-item-text"> '+row.task_desc+'. </div>';
                            htmlview += '<div class="todo-tasklist-controls pull-left">';
                                htmlview += '<span class="todo-tasklist-date">';
                                    htmlview += '<i class="fa fa-calendar"></i> '+row.task_date+' </span>';
                                htmlview += '<span class="todo-tasklist-badge badge badge-roundless">'+row.name+'</span> ';
                                htmlview += '<span class="todo-tasklist-badge badge badge-roundless bg-blue-steel">'+ new Intl.NumberFormat().format(result.project.total_nominal)+'(IDR)</span>';
                            htmlview += '</div>';
                        htmlview += '</div>';
                    
                        }
                    
                                             
                }else{
                    
                }
                $('#todo-tasklist-id').html(htmlview);   
              }
        });
    });
})

</script>
@endsection    