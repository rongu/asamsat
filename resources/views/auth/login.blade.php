@extends('layouts.app-login')

@section('content')
<!-- BEGIN LOGO -->

<div class="logo">
            <a href="#">
                 </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
                <center><img src="{{ asset('images/logo-header.png') }}" style="height: 50px; width: auto;" alt="" /></center>
            
            <form class="login-form" method="POST" action="{{ route('login') }}">
                    @csrf
                <h3 class="form-title">Login</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">

                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Phone/Email</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix @error('email') is-invalid @enderror" type="text" autocomplete="off" placeholder="Phone / Email" name="email" value="{{ old('email') }}" /> </div>
                </div>
                <div class="form-group">

                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix @error('password') is-invalid @enderror" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                </div>
                <div class="form-actions">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /> Remember me
                        <span></span>
                    </label>
                    <button type="submit" class="btn green pull-right"> Login </button>
                </div>


                <div class="create-account">
                    <p> Tidak punya akun ?&nbsp;
                        <a href="javascript:;" id="register-btn"> Daftar </a>
                    </p>
                </div>
            </form>

            <form id="register-form" class="register-form" method="post" action="{{ route('register') }}">
                @csrf
                <h3>Pendaftaran</h3>
                <p> Masukan Data Diri Anda pada isian di bawah : </p>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Nama</label>
                    <div class="input-icon">
                        <i class="fa fa-font"></i>
                        <input class="form-control placeholder-no-fix @error('name') is-invalid @enderror" type="text" placeholder="Nama" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus/> </div>
                </div>

                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix @error('email') has-error @enderror" type="email" name="email" autocomplete="off" placeholder="Email" value="{{ old('email') }}" autocomplete="email" /> </div>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>

                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Nomor HP</label>
                    <div class="input-icon">
                        <i class="fa fa-phone"></i>
                         <input class="form-control placeholder-no-fix @error('phone') has-error @enderror" type="text" name="phone" autocomplete="off" placeholder="phone" value="{{ old('phone') }}" required autocomplete="phone" />  </div>
                    @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">NIK</label>
                    <div class="input-icon">
                        <i class="fa fa-credit-card"></i>
                         <input class="form-control placeholder-no-fix @error('nik') has-error @enderror" type="text" name="nik" autocomplete="off" placeholder="nik" value="{{ old('nik') }}" required autocomplete="nik" />  </div>
                    @error('nik')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Alamat</label>
                    <div class="input-icon">
                        <i class="fa fa-home"></i>
                         <textarea class="form-control placeholder-no-fix @error('alamat') has-error @enderror" name="alamat" autocomplete="off" required rows="3" style="margin-bottom: 15px;">{{ old('alamat') }}</textarea>
                         <!-- <input class="form-control placeholder-no-fix @error('alamat') has-error @enderror" type="text" name="alamat" autocomplete="off" placeholder="alamat" value="{{ old('alamat') }}" required autocomplete="alamat" />  </div> -->
                    @error('alamat')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                

                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input id="password" type="password" class="form-control placeholder-no-fix @error('password') has-error @enderror" name="password"  placeholder="Password" required autocomplete="new-password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
                    <div class="controls">
                        <div class="input-icon">
                            <i class="fa fa-check"></i>
                            <input id="password-confirm" type="password" class="form-control placeholder-no-fix" name="password_confirmation" required placeholder="Confirm Password" autocomplete="new-password">
                        </div>
                    </div>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror  
                </div>

                <div class="form-actions">
                    <button id="register-back-btn" type="button" class="btn grey-salsa btn-outline"> Back </button>
                    <button type="submit" id="register-submit-btn" class="btn green pull-right"> Sign Up </button>
                </div>
            </form>
            <!-- END REGISTRATION FORM -->
        </div>
@endsection
