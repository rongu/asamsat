@extends('layouts.application')

@section('content')


<div class="container">
            <div class="card bg-template shadow mt-4 h-100">
                <div class="card-body">
                    <div class="row">
                        <div class="col-auto">
                        	
	                            <figure class="avatar avatar-60">
	                            	<a href="{{route('go.apps.profile')}}" class="btn btn-link-default"  style="padding: 0px; margin: 0px;">
	                            		<div class="circle2">{{$profil->inisial}}</div>
	                            	</a>
	                            </figure>

                        </div>
                        <div class="col pl-0 align-self-center">
                            <h5 class="mb-1">{{Auth::user()->name}}</h5>
                            <p class="text-mute small">{{Auth::user()->phone}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container">

            <div class="row text-center mt-4">
                <div class="col-12 col-md-12">
                    <div class="card shadow border-0 mb-3">
                        <a href="{{route('go.apps.antrian')}}" class="btn btn-link-default">
	                        <div class="card-body">
	                            <div class="avatar avatar-60 no-shadow border-0">
	                                <div class="overlay bg-template"></div>
	                                <i class="material-icons vm md-36 text-template">touch_app</i>
	                            </div>
	                            <h3 class="mt-3 mb-0 font-weight-normal">Info Antrian</h3>
	                        </div>
	                    </a>
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="card shadow border-0 mb-3">
                        <a href="{{route('go.apps.ambilAntrian')}}" class="btn btn-link-default">
                            <div class="card-body">
                                <div class="avatar avatar-60 no-shadow border-0">
                                    <div class="overlay bg-template"></div>
                                    <i class="material-icons vm md-36 text-template">queue</i>
                                </div>
                                <h3 class="mt-3 mb-0 font-weight-normal">Pendaftaran Antrian</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="card shadow border-0 mb-3">
                        <a href="{{route('go.apps.history')}}" class="btn btn-link-default">
                        <div class="card-body">
                            
                                <div class="avatar avatar-60 no-shadow border-0">
                                <div class="overlay bg-template"></div>
                                <i class="material-icons vm md-36 text-template">history</i>
                                </div>
                                <h3 class="mt-3 mb-0 font-weight-normal">Reg. History</h3>
                                                     
                        </div>
                        </a>   
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="card shadow border-0 mb-3">
                        <a href="{{route('go.apps.setting')}}" class="btn btn-link-default">
                        <div class="card-body">
                            
                                <div class="avatar avatar-60 no-shadow border-0">
                                <div class="overlay bg-template"></div>
                                <i class="material-icons vm md-36 text-template">phonelink_setup</i>
                                </div>
                                <h3 class="mt-3 mb-0 font-weight-normal">Pengaturan</h3>
                                                     
                        </div>
                        </a>   
                    </div>
                </div>
            </div>
        </div>

@endsection