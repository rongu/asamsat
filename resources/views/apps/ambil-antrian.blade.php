@extends('layouts.application')

@section('content')

    <div class="container">
            <div class="card bg-template shadow mt-4 h-50">
                <div class="card-body">
                    <div class="row">
                        <div class="col pl-0 align-self-center">
                             <a href="{{route('go.apps')}}"><i class="material-icons float-left">&nbsp;&nbsp;&nbsp;arrow_back</i></a>
                            <center><h5 class="mb-1">Ambil Antrian</h5></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- notification -->
        @if(session('success'))                                        
        <div class="notification bg-white shadow-sm border-primary active">
            <div class="row">
                <div class="col-auto align-self-center pr-0">
                    <i class="material-icons text-primary md-36">fullscreen</i>
                </div>
                <div class="col">
                    <h6>{{ session('success') }}</h6>
                    <p class="mb-0 text-secondary">Silahkan cek di Reg. History Anda.</p>
                </div>
                <div class="col-auto align-self-center pl-0">
                    <button class="btn btn-link closenotification"><i class="material-icons text-secondary text-mute md-18 ">close</i></button>
                </div>
            </div>
        </div>
        @endif

        <!-- notification ends -->

        <div class="container">
            <div class="row text-center mt-4">
                <div class="col-12 col-md-12">
                    @if(session('error'))
                        <div class="alert alert-danger display-show">
                        <button class="close" data-close="alert"></button>  {{ session('error') }} </div>
                    @endif
                </div>
                <div class="col-12 col-md-12">
                    <div class="card shadow border-0 mb-3">
                        <div class="card-body">
                            <div class="row">
                                <form id="getNomorAntrian" class="form-horizontal col-12 col-md-12 col-lg-12" role="form" action="{{route('go.apps.antrianCreate')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Jenis Pelayanan</label>
                                            <select class="form-control" id="jenis_pelayanan_id" name="jenis_pelayanan">
                                                @foreach($jpl as $val)
                                                    <option value="{{$val->id}}">{{$val->nama_pelayanan}}</option>
                                                @endforeach
                                            </select>
                                            <div class="form-group mt-4">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Tanggal Pelayanan</label>
                                            <input type="date" id="tanggal_pelayanan_id" name="tanggal_pelayanan" class="date-picker form-control text-center" required="" >
                                            <div class="form-group mt-4">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <button type="submit" class="shadow mr-12 btn btn-primary rounded mb-12">Ambil Nomor Antrian</button>
                                            <div class="form-group mt-4">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>

@endsection

@section('assets-bottom')
    <script type="text/javascript">
        $(document).ready(function() {

            var jns = $("#jenis_pelayanan_id").val();

            var rdy;
            $.ajax({
                url: "{{ url('apps-antrian-ready') }}/"+jns,
                type: "GET",
                dataType: "JSON",
                async:false,
                success: function(datas) {
                //console.dir(datas);
                rdy = datas;
                return rdy;
                }   
            });

            setTimeout(function(){ 
                console.log(rdy.date_s);

                var date = new Date(rdy.date_s);
                var yyyy = date.getFullYear();
                var mm = date.getMonth();
                var dd_min = date.getDate();
                var dd_max = date.getDate()+1;
                min = [date.getFullYear(),('0' + (date.getMonth() + 1)).slice(-2),('0' + dd_min).slice(-2)].join('-');
                max = [date.getFullYear(),('0' + (date.getMonth() + 1)).slice(-2),('0' + dd_max).slice(-2)].join('-');
                document.getElementById("tanggal_pelayanan_id").setAttribute("max", max);
                document.getElementById("tanggal_pelayanan_id").setAttribute("min", min);
            }, 500);

            
        });
    </script>
@endsection 


