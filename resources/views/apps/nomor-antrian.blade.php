@extends('layouts.application')

@section('content')


<div class="container">
            <div class="card bg-template shadow mt-4 h-50">
                <div class="card-body">
                    <div class="row">
                        <div class="col pl-0 align-self-center">
                             <a href="{{route('go.apps')}}"><i class="material-icons float-left">&nbsp;&nbsp;&nbsp;arrow_back</i></a>
                            <center><h5 class="mb-1">Info Antrian</h5></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row text-center mt-4">
                <div class="col-12 col-md-12">
                    <div class="card shadow border-0 mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto pr-0">
                                    <figure class="avatar avatar-80 border-0">
                                        <img src="{{ asset('images/sim_baru.png') }}" alt="">
                                    </figure>
                                </div>
                                <div class="col align-self-center">
                                    <h4>Pelayanan SIM Baru</h4>
                                    @if ($sim_baru)
                                    <h5>Nomor Urut : {{$sim_baru->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sim_baru->nomor_antrian)}}</h5>
                                    Total Antrian : {{$sim_baru->total}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="card shadow border-0 mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto pr-0">
                                    <figure class="avatar avatar-80 border-0">
                                        <img src="{{ asset('images/sim_perpanjang.png') }}" alt="">
                                    </figure>
                                </div>
                                <div class="col align-self-center">
                                    <h4>Pelayanan SIM Perpanjangan</h4>
                                    @if ($sim_perpanjang)
                                    <h5>Nomor Urut : {{$sim_perpanjang->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sim_perpanjang->nomor_antrian)}}</h5>
                                    Total Antrian : {{$sim_perpanjang->total}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="card shadow border-0 mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto pr-0">
                                    <figure class="avatar avatar-80 border-0">
                                        <img src="{{ asset('images/skck_baru.png') }}" alt="">
                                    </figure>
                                </div>
                                <div class="col align-self-center">
                                    <h4>Pelayanan SKCK Baru</h4>
                                    @if ($skck_baru)
                                    <h5>Nomor Urut : {{$skck_baru->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($skck_baru->nomor_antrian)}}</h5>
                                    Total Antrian : {{$skck_baru->total}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="card shadow border-0 mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto pr-0">
                                    <figure class="avatar avatar-80 border-0">
                                        <img src="{{ asset('images/skck_perpanjang.png') }}" alt="">
                                    </figure>
                                </div>
                                <div class="col align-self-center">
                                    <h4>Pelayanan SKCK Perpanjangan</h4>
                                    @if ($skck_perpanjang)
                                    <h5>Nomor Urut : {{$skck_perpanjang->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($skck_perpanjang->nomor_antrian)}}</h5>
                                    Total Antrian : {{$skck_perpanjang->total}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="card shadow border-0 mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto pr-0">
                                    <figure class="avatar avatar-80 border-0">
                                        <img src="{{ asset('images/rekam_sidik_jari.png') }}" alt="">
                                    </figure>
                                </div>
                                <div class="col align-self-center">
                                    <h4>Perekaman Sidik Jari</h4>
                                    @if ($sidik_jari)
                                    <h5>Nomor Urut : {{$sidik_jari->kode_antrian}} - {{App\Http\Controllers\ApplicationController::getUrut($sidik_jari->nomor_antrian)}}</h5>
                                    Total Antrian : {{$sidik_jari->total}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection