@extends('layouts.application')

@section('content')

    <div class="container">
            <div class="card bg-template shadow mt-4 h-50">
                <div class="card-body">
                    <div class="row">
                        <div class="col pl-0 align-self-center">
                             <a href="{{route('go.apps')}}"><i class="material-icons float-left">&nbsp;&nbsp;&nbsp;arrow_back</i></a>
                            <center><h5 class="mb-1">Profil Pengguna</h5></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row text-center mt-4">
                <div class="col-12 col-md-12">
                    @if(session('update'))
                        <div class="alert alert-success display-show">
                        <button class="close" data-close="alert"></button>  {{ session('update') }} </div>
                    @endif
                </div>
                <div class="col-12 col-md-12">
                    @if(session('error_konfirm'))
                        <div class="alert alert-warning display-show">
                        <button class="close" data-close="alert"></button>  {{ session('error_konfirm') }} </div>
                    @endif
                </div>
                <div class="col-12 col-md-12">
                    @if(session('succes_update_password'))
                        <div class="alert alert-success display-show">
                        <button class="close" data-close="alert"></button>  {{ session('succes_update_password') }} </div>
                    @endif
                </div>
                <div class="col-12 col-md-12">
                    <div class="card shadow border-0 mb-3">
                        <div class="card-body">
                            <form id="getUpdateprofile" class="form-horizontal col-12 col-md-12 col-lg-12" role="form" action="{{route('go.apps.updateProfile')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                @if ($profil)                                    
                                <div class="text-center">
                                    <div class="figure-profile shadow my-4">
                                        <div class="circle">{{$profil->inisial}}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 mx-auto text-center">
                                        <div class="form-group float-label active mb-0">
                                            <input type="text" class="form-control" id="nik_id" name="nik" required="" value="{{$profil->nik}}" maxlength="18" required="">
                                            <label class="form-control-label">NIK</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 mx-auto text-center">
                                        <div class="form-group float-label active">
                                            <input type="text" class="form-control" id="nama_id" name="nama" required="" value="{{$profil->name}}" maxlength="50" required="">
                                            <label class="form-control-label">Nama</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 mx-auto text-center">
                                        <div class="form-group float-label active mb-0">
                                            <input type="tel" class="form-control" id="alamat_id" name="alamat" required="" value="{{$profil->alamat}}" maxlength="150" required="">
                                            <label class="form-control-label">Alamat</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 mx-auto text-center">
                                        <div class="form-group float-label active mb-0">
                                            <input type="tel" class="form-control" id="telpon_id" name="telpon" required="" value="{{$profil->phone}}" maxlength="14" required="">
                                            <label class="form-control-label">Nomor Telepon</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 mx-auto text-center">
                                        <div class="form-group float-label active">
                                            <input type="email" class="form-control" id="email_id" name="email" required="" value="{{$profil->email}}">
                                            <label class="form-control-label">Email</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 mx-auto text-center">
                                        <button type="submit" class="shadow mr-12 btn btn-primary rounded mb-12">Ubah Data Profil</button>
                                    </div>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                    <div class="card shadow border-0 mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 mx-auto text-center">
                                    <h4 class="mt-5"><span class="font-weight-light">Ubah </span>Password</h4>
                                    <br>
                                    <form id="getUpdateprofile" class="form-horizontal col-12 col-md-12 col-lg-12" role="form" action="{{route('go.apps.updatePassword')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-group float-label">
                                            <input type="password" id="inputPassword1" name="inputPassword1" class="form-control form-control-lg" required="">
                                            <label for="inputPassword1" class="form-control-label">Password Baru</label>
                                        </div>
                                        <div class="form-group float-label">
                                            <input type="password" id="inputPassword2" name="inputPassword2" class="form-control form-control-lg" required="">
                                            <label for="inputPassword2" class="form-control-label">Konfirmasi Password Baru</label>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col">
                                                <button type="submit" class="shadow mr-12 btn btn-primary rounded mb-12">Ubah Password</button>
                                            </div>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>

@endsection
