@extends('layouts.app-template')

@section('assets-top')
<style type="text/css">
    
    .multiselect-selected-text{
        float: left;
    }

    .multiselect>.caret{
        float: right;
    }

    .dataTables_scrollBody{
        min-height: 51px;
    }
</style>
@endsection

@section('content')


<!-- BEGIN PAGE HEADER-->
<!-- BEGIN THEME PANEL -->

<!-- END THEME PANEL -->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Users</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Setup Antrian Online
    <small>statistics, charts, recent events and reports</small>
</h1>

<div class="clearfix"></div>

<form id="myProject" role="form" action="{{route('setup.update')}}" method="post" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="portlet light ">
    <!-- PROJECT HEAD -->
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bar-chart font-green-sharp hide"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Setup Antrian Online</span>
            
        </div>

    </div>
    <!-- end PROJECT HEAD -->
    <div class="portlet-body">
        <div class="row">
            <div class="col-12 col-md-12">
            @if(session('ubahsetup'))
            <div class="alert alert-success display-show">
            <button class="close" data-close="alert"></button>  {{ session('ubahsetup') }} </div>
            @endif
            </div>
            <div class="col-lg-4">
                <div class="portlet mt-element-ribbon light portlet-fit bordered">
                    <div class="ribbon ribbon-right ribbon-clip ribbon-shadow ribbon-border-dash-hor ribbon-color-success uppercase">
                        <div class="ribbon-sub ribbon-clip ribbon-right bg-red-intense bg-font-red-intense"></div> SIM BARU </div>
                    
                    <div class="portlet-body"> <input class="knob" value="{{$in_p_s_b->batasan}}" name="p_s_b" data-angleoffset=-125 data-anglearc=250 data-fgcolor="#e35b5a" value="2" data-min="1" data-max="200">  </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="portlet mt-element-ribbon light portlet-fit bordered">
                    <div class="ribbon ribbon-right ribbon-clip ribbon-shadow ribbon-border-dash-hor ribbon-color-success uppercase">
                        <div class="ribbon-sub ribbon-clip ribbon-right bg-yellow-gold bg-font-yellow-gold"></div> SIM PERPANJANG </div>
                    
                    <div class="portlet-body"> <input class="knob" value="{{$in_p_s_p->batasan}}" name="p_s_p" data-angleoffset=-125 data-anglearc=250 data-fgcolor="#e87e04" value="2" data-min="1" data-max="200">  </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="portlet mt-element-ribbon light portlet-fit bordered">
                    <div class="ribbon ribbon-right ribbon-clip ribbon-shadow ribbon-border-dash-hor ribbon-color-success uppercase">
                        <div class="ribbon-sub ribbon-clip ribbon-right bg-grey-salsa bg-font-grey-salsa"></div> SKCK BARU </div>
                    
                    <div class="portlet-body"> <input class="knob" value="{{$in_p_sk_b->batasan}}" name="p_sk_b" data-angleoffset=-125 data-anglearc=250 data-fgcolor="#acb5c3" value="2" data-min="1" data-max="200">  </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="portlet mt-element-ribbon light portlet-fit bordered">
                    <div class="ribbon ribbon-right ribbon-clip ribbon-shadow ribbon-border-dash-hor ribbon-color-success uppercase">
                        <div class="ribbon-sub ribbon-clip ribbon-right bg-green-sharp bg-font-green-sharp"></div> SKCK PERPANJANG </div>
                    
                    <div class="portlet-body"> <input class="knob" value="{{$in_p_sk_p->batasan}}" name="p_sk_p" data-angleoffset=-125 data-anglearc=250 data-fgcolor="#2ab4c0" value="2" data-min="1" data-max="200">  </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="portlet mt-element-ribbon light portlet-fit bordered">
                    <div class="ribbon ribbon-right ribbon-clip ribbon-shadow ribbon-border-dash-hor ribbon-color-success uppercase">
                        <div class="ribbon-sub ribbon-clip ribbon-right bg-blue-sharp bg-font-blue-sharp"></div> REKAM SIDIK JARI </div>
                    
                    <div class="portlet-body"> <input class="knob" value="{{$in_p_r_s->batasan}}" name="p_r_s" data-angleoffset=-125 data-anglearc=250 data-fgcolor="#5c9bd1" value="2" data-min="1" data-max="200">  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="portlet-footer">
        <button type="submit" class="btn btn-success btn-lg">Simpan</button>
    </div>
</div>
</form>

@endsection


@section('assets-bottom')
<script>
$(document).ready(function() {

    $(".knob").knob({
        'dynamicDraw': true,
        'thickness': 0.2,
        'tickColorizeValues': true,
        //'skin': 'tron',
    }); 

    
})

</script>
@endsection    