@extends('layouts.app-template')

@section('assets-top')

@section('assets-top')
<style type="text/css">
    
    .multiselect-selected-text{
        float: left;
    }

    .multiselect>.caret{
        float: right;
    }

    .multiselect-container.dropdown-menu>li>a {
        padding: 0px 30px;
        color: #6f6f6f;
        text-decoration: none;
        display: block;
        clear: both;
        font-weight: 300;
        line-height: 18px;
        white-space: nowrap;
    }

    .dataTables_scrollBody{
        min-height: 51px;
    }
</style>
@endsection 

@section('content')


<!-- BEGIN PAGE HEADER-->
<!-- BEGIN THEME PANEL -->

<!-- END THEME PANEL -->
<!-- BEGIN PAGE BAR -->

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Privilege</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Privilege Dashboard
    <small>statistics, charts, recent events and reports</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS 1-->

<div class="portlet light ">
    <!-- PROJECT HEAD -->
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bar-chart font-green-sharp hide"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Data Privilege</span>
            
        </div>

    </div>
    <!-- end PROJECT HEAD -->
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-7 col-sm-12">
                
                @if(session('update'))
                <div class="alert alert-success display-show">
                <button class="close" data-close="alert"></button> {{ session('update') }} </div>
                @endif

                <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_3">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Level</th>
                            <th>Access</th>
                            <th>&nbsp;</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        
                   </tbody>
                </table>
            </div>
            <div class="todo-tasklist-devider"> </div>
            <div class="col-md-5 col-sm-12">
               
               @if(session('success'))
                <div class="alert alert-success display-show">
                <button class="close" data-close="alert"></button> {{ session('success') }} </div>
                @endif

                @if(session('error'))
                <div class="alert alert-danger display-show">
                <button class="close" data-close="alert"></button>  {{ session('error') }} </div>
                @endif
                <form id="myPrivilege" class="" role="form" action="{{ route('privilege.create') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <input id="roles_id" type="hidden" class="form-control" name="id" value="0">

                    <div class="form-body" style="height: 495px;">
                        
                        <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="name_role_id" name="name_role">
                            <label for="name_role_id">Name</label>
                        </div>

                        <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="level_role_id" name="level_role">
                            <label for="level_role_id">Level</label>
                        </div>
                        

                        <div class="form-group form-md-line-input">
                            <label for="mt_access">Access</label>
                            <select id="mt_access" name="access_id[]" multiple="multiple" class="form-control btn btn-default" data-clickable-groups="false" data-collapse-groups="true" data-width="100%" data-height="200" data-filter="true" data-select-all="true" data-action-onchange="true" data-label="left">
                                @foreach($access as $key)
                                    <optgroup label="{{$key->name}}" class="group-{{$key->id}}">
                                        <option value="{{$key->id}}">{{$key->name}}</option>
                                        @php
                                        $datas = App\Http\Controllers\PrivilegeController::getChild($key->id);

                                        @endphp

                                        @foreach($datas as $vl)
                                            <option value="{{$vl->id}}">{{$vl->name}}</option>
                                        @endforeach

                                    </optgroup>
                                    
                                @endforeach
                            </select>

                        </div>   
                       
                    </div>
                    <div class="form-actions right">
                        <button type="submit" class="btn green btn-sm">Submit</button>
                        <button type="reset" class="btn default btn-sm">Reset</button>
                        <a id="delete_btn" class="btn red-mint btn-outline sbold uppercase btn-sm">Delete</a>
                    </div>
                </form>
                        
            </div>
        </div>
    </div>
</div>

@endsection


@section('assets-bottom')
<script>
$(document).ready(function() {

    if (jQuery().datepicker) {
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "left",
            autoclose: true
        });
        //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
    }

    $("#delete_btn").hide();

    var form1 = $('#myPrivilege');
    var error1 = $('.alert-danger', form1);
    var success1 = $('.alert-success', form1);

    form1.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        // nik_em name_em email_em phones_em address_em position_em about_em
        rules: {
            
            name_role: {
                required: true
            },

            level_role: {
                required: true
            },


        },

        invalidHandler: function(event, validator) { //display error alert on form submit              
            success1.hide();
            error1.show();
            App.scrollTo(error1, -200);
        },

        errorPlacement: function(error, element) {
            if (element.is(':checkbox')) {
                error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
            } else if (element.is(':radio')) {
                error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        highlight: function(element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function(element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function(label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function(form) {
            success1.show();
            error1.hide();
            form1[0].submit(); // submit the form

        }
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    var table = $('#sample_3');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{ route('privilege.list') }}",                  
    },
    scrollY:        400,
    deferRender:    true,
    scroller:       true,
    deferRender:    true,
    scrollX:        true,
    scrollCollapse: true,        
    responsive:     true,  
    stateSave:      true,
    columnDefs: [
            { orderable: false, targets: [4] },
            { searchable : false},                

            { render: function(data, type, row, meta) {
                // id, name, email, phones, position, nik, address, about
                return '<a href="javascript:;" data-id="'+data[0]+'" data-name="'+data[1]+'" data-level="'+data[2]+'" data-access="'+data[3]+'" class="edit_btn btn btn-circle btn-icon-only grey-mint"><i class="fa fa-edit"></i></a>'; }, targets: [4]
            },
    ],
    columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'name', name: 'name' },
            { data: 'level', name: 'level' },
            { data: 'access', name: 'access' },
            { data: 'roles_id', name: 'roles_id' }  
            
        ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    });

    $("#sample_3").on('click','.edit_btn',function(e){
        // id, name, email, phones, position, nik, address, about
        var id = e.currentTarget.dataset.id;
        var name = e.currentTarget.dataset.name;
        var level = e.currentTarget.dataset.level;
        var access = e.currentTarget.dataset.access;
  
        $("#roles_id").val(id);
        $("#name_role_id").val(name);
        $("#level_role_id").val(level);
        $('#mt_access').val([]).multiselect('refresh');
        $("#mt_access").multiselect('refresh'); 
        $("#mt_access").multiselect('select',JSON.parse(access));
        $("#mt_access").multiselect('rebuild');

        $("#delete_btn").show();
    });

    $("#delete_btn").on('click',function(e){
        var id = $("#roles_id").val();
        console.log(id);
        //console.log(sa_btnClass);
        swal({
          title: "Apakah akan menghapus data Privilege Ini?",
          text: "informasi Privilege akan hilang dari web, apakah anda yakin akan melakukan proses ini...",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Setuju hapus",
          cancelButtonText: "Tidak, Batal hapus",
        },function(isConfirm){
                if (isConfirm){
                    $.ajax({
                        url: "{{ url('privilege-delete') }}/"+id,
                        type: "GET",
                        dataType: "JSON",
                        success: function(data) {
                            if(data){
                                swal("Terimakasih", "Anda telah menghapus data", "success");
                            }else{
                                swal("Maaf!!!", "User Tidak Memiliki akses Hapus", "error");
                            }
                            location.reload(true);
                        }
                    });
                    
                } else {
                    swal("Batal", "Anda membatalkan proses hapus Employee...", "error");
                }
        });
    });

    $('#mt_access').each(function(){
        var btn_class = $(this).attr('class');
        var clickable_groups = ($(this).data('clickable-groups')) ? $(this).data('clickable-groups') : false ;
        var collapse_groups = ($(this).data('collapse-groups')) ? $(this).data('collapse-groups') : false ;
        var drop_right = ($(this).data('drop-right')) ? $(this).data('drop-right') : false ;
        var drop_up = ($(this).data('drop-up')) ? $(this).data('drop-up') : false ;
        var select_all = ($(this).data('select-all')) ? $(this).data('select-all') : false ;
        var width = ($(this).data('width')) ? $(this).data('width') : '' ;
        var height = ($(this).data('height')) ? $(this).data('height') : '' ;
        var filter = ($(this).data('filter')) ? $(this).data('filter') : false ;

        // advanced functions
        var onchange_function = function(option, checked, select) {
            // alert('Changed option ' + $(option).val() + '.');
        }
        var dropdownshow_function = function(event) {
            // alert('Dropdown shown.');
        }
        var dropdownhide_function = function(event) {
            // alert('Dropdown Hidden.');
        }

        // init advanced functions
        var onchange = ($(this).data('action-onchange') == true) ? onchange_function : '';
        var dropdownshow = ($(this).data('action-dropdownshow') == true) ? dropdownshow_function : '';
        var dropdownhide = ($(this).data('action-dropdownhide') == true) ? dropdownhide_function : '';

        // template functions
        // init variables
        var li_template;
        if ($(this).attr('multiple')){
            li_template = '<li class="mt-checkbox-list"><a href="javascript:void(0);"><label class="mt-checkbox"> <span></span></label></a></li>';
        } else {
            li_template = '<li><a href="javascript:void(0);"><label></label></a></li>';
        }

        // init multiselect
        $(this).multiselect({
            enableClickableOptGroups: clickable_groups,
            enableCollapsibleOptGroups: collapse_groups,
            enableCaseInsensitiveFiltering: true,
            disableIfEmpty: true,
            enableFiltering: filter,
            includeSelectAllOption: select_all,
            dropRight: drop_right,
            buttonWidth: width,
            maxHeight: height,
            onChange: onchange,
            onDropdownShow: dropdownshow,
            onDropdownHide: dropdownhide,
            buttonClass: btn_class,
            nonSelectedText: "Pilih Access",

            //optionClass: function(element) { return "mt-checkbox"; },
            //optionLabel: function(element) { console.log(element); return $(element).html() + '<span></span>'; },
            /*templates: {
                li: li_template,
            }*/
        });   
    });

})

</script>
@endsection    