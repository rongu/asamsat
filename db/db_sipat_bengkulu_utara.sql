-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2021 at 05:02 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_asamsat`
--

-- --------------------------------------------------------

--
-- Table structure for table `audio_visual`
--

CREATE TABLE `audio_visual` (
  `id` int(11) NOT NULL,
  `footnote` varchar(255) DEFAULT NULL,
  `link_video` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `audio_visual`
--

INSERT INTO `audio_visual` (`id`, `footnote`, `link_video`, `created_at`, `updated_at`) VALUES
(1, 'periksa kelengkapan dokumen anda sebelum mengambil antrian', 'video-1.mkv', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dt_antrian`
--

CREATE TABLE `dt_antrian` (
  `id` bigint(20) NOT NULL,
  `md_pelayanan_id` int(11) DEFAULT NULL,
  `users_id` bigint(20) DEFAULT NULL,
  `tanggal_pelayanan` date DEFAULT NULL,
  `nomor_antrian` int(11) DEFAULT NULL,
  `status_pelayanan` enum('SELESAI','MENUNGGU') DEFAULT 'MENUNGGU',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `phones` varchar(20) NOT NULL,
  `position` varchar(20) NOT NULL,
  `nik` varchar(10) DEFAULT NULL,
  `about` text NOT NULL,
  `users_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `email` varchar(100) DEFAULT NULL,
  `imgs` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `address`, `phones`, `position`, `nik`, `about`, `users_id`, `created_at`, `updated_at`, `email`, `imgs`) VALUES
(1, 'Admin', 'Jl. Ambon', '09999999', 'Leader', '31111', 'me', 1, '2021-02-13 06:44:32', '2021-02-12 23:44:32', 'admin@mail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `md_pelayanan`
--

CREATE TABLE `md_pelayanan` (
  `id` int(11) NOT NULL,
  `nama_pelayanan` varchar(255) DEFAULT NULL,
  `kode_antrian` char(1) DEFAULT NULL,
  `batasan` int(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `md_pelayanan`
--

INSERT INTO `md_pelayanan` (`id`, `nama_pelayanan`, `kode_antrian`, `batasan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SIM BARU', 'A', 11, NULL, '2021-03-24 15:07:23', NULL),
(2, 'SIM PERPANJANG', 'B', 50, NULL, '2021-03-24 15:07:23', NULL),
(3, 'SKCK BARU', 'C', 67, NULL, '2021-03-24 15:07:23', NULL),
(4, 'SKCK PERPANJANG', 'D', 70, NULL, '2021-03-24 15:07:23', NULL),
(5, 'REKAM SIDIK JARI', 'E', 62, NULL, '2021-03-24 15:07:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2016_01_15_105324_create_roles_table', 2),
(5, '2016_01_15_114412_create_role_user_table', 2),
(6, '2016_01_26_115212_create_permissions_table', 2),
(7, '2016_01_26_115523_create_permission_role_table', 2),
(8, '2016_02_09_132439_create_permission_user_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `model`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Halaman User', 'users.page', 'Untuk akses menuju halaman User', 'Permission', '2021-02-13 06:08:49', '2021-02-13 06:08:58', NULL),
(2, 'Halaman Rekap Pendaftaran', 'rekap.page', 'Untuk akses halaman Rekap pendaftaran', 'Permission', '2021-02-13 06:08:49', '2021-02-13 06:08:49', NULL),
(3, 'Halaman Privilege', 'privilege.page', 'Untuk akses menuju halaman Privilege', 'Permission', '2021-02-13 06:08:49', '2021-02-13 06:08:58', NULL),
(4, 'Bisa Tambah User', 'users.add', 'Untuk akses Tambah User', 'Permission', '2021-02-13 06:08:49', '2021-02-13 06:08:58', NULL),
(5, 'Bisa Edit User', 'users.edit', 'Untuk akses Edit User', 'Permission', '2021-02-13 06:08:49', '2021-02-13 06:08:58', NULL),
(6, 'Bisa Hapus User', 'users.delete', 'Untuk akses Hapus User', 'Permission', '2021-02-13 06:08:49', '2021-02-13 06:08:58', NULL),
(7, 'Bisa Tambah Privilege', 'privilege.add', 'Untuk akses Tambah Privilege', 'Permission', '2021-02-13 06:08:49', '2021-02-13 06:08:58', NULL),
(8, 'Bisa Edit Privilege', 'privilege.edit', 'Untuk akses Edit Privilege', 'Permission', '2021-02-13 06:08:49', '2021-02-13 06:08:58', NULL),
(9, 'Bisa Hapus Privilege', 'privilege.delete', 'Untuk akses Hapus Privilege', 'Permission', '2021-02-13 06:08:49', '2021-02-13 06:08:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions_groups`
--

CREATE TABLE `permissions_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `permissions_id` int(10) NOT NULL,
  `childs_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions_groups`
--

INSERT INTO `permissions_groups` (`id`, `permissions_id`, `childs_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '[\"4\",\"5\",\"6\"]', '2021-02-19 08:29:13', NULL, NULL),
(2, 2, '[]', '2021-02-19 08:29:30', NULL, NULL),
(3, 3, '[\"7\",\"8\",\"9\"]', '2021-02-19 08:29:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2021-03-26 01:07:31', '2021-03-26 01:07:31', NULL),
(2, 2, 1, '2021-03-26 01:07:31', '2021-03-26 01:07:31', NULL),
(3, 3, 1, '2021-03-26 01:07:31', '2021-03-26 01:07:31', NULL),
(4, 4, 1, '2021-03-26 01:07:31', '2021-03-26 01:07:31', NULL),
(5, 5, 1, '2021-03-26 01:07:31', '2021-03-26 01:07:31', NULL),
(6, 6, 1, '2021-03-26 01:07:31', '2021-03-26 01:07:31', NULL),
(7, 7, 1, '2021-03-26 01:07:31', '2021-03-26 01:07:31', NULL),
(8, 8, 1, '2021-03-26 01:07:31', '2021-03-26 01:07:31', NULL),
(9, 9, 1, '2021-03-26 01:07:31', '2021-03-26 01:07:31', NULL),
(11, 2, 4, '2021-03-26 01:15:26', '2021-03-26 01:15:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `level`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin', 'Admin Role', 5, '2021-02-04 12:47:35', '2021-02-04 12:47:35', NULL),
(2, 'Users', 'users', 'Users Role', 1, '2021-02-28 01:13:35', '2021-02-28 01:13:35', NULL),
(3, 'Guest', 'guest', 'Guest Role', 0, '2021-02-04 12:47:35', '2021-02-28 01:13:25', NULL),
(4, 'Operator', 'operator', 'Operator Role', 2, '2021-02-28 19:49:11', '2021-02-28 19:49:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, NULL, NULL, NULL),
(2, 3, 2, '2021-03-26 01:06:08', '2021-03-26 01:06:17', NULL),
(3, 4, 3, '2021-03-26 01:06:59', '2021-03-26 01:06:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nik` varchar(18) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `nik`, `alamat`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'admin@mail.com', '12345678', '00000', 'Jl. Padang - Bengkulu, Ps. Baru Kota Bani, Putri Hijau, Kabupaten Bengkulu Utara, Bengkulu 38326', NULL, '$2y$10$6NwWa87SVvO23uspcgNqTu6ndl1aFOee/T9khSYxKazv0HLLaGY8u', NULL, '2021-02-28 00:37:28', '2021-03-13 00:54:21'),
(2, 'guest', 'guest@mail.com', '11111', '11111', '-', NULL, '$2y$10$CauP2c8waMO1v7dVHx.sy.ynChBzKxGUypkre8dTftA7CPvqaREMW', NULL, '2021-03-26 01:06:08', '2021-03-26 01:07:13'),
(3, 'operator', 'operator@mail.com', '22222', '22222', '-', NULL, '$2y$10$4bm5UtPtm7FQK1TMcq2DP.oPBKlLDGiEtrTR8f2WDX2K0sXhBFOPe', NULL, '2021-03-26 01:06:59', '2021-03-26 01:06:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audio_visual`
--
ALTER TABLE `audio_visual`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_antrian`
--
ALTER TABLE `dt_antrian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `md_pelayanan`
--
ALTER TABLE `md_pelayanan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `permissions_groups`
--
ALTER TABLE `permissions_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_index` (`permission_id`),
  ADD KEY `permission_user_user_id_index` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phones` (`phone`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audio_visual`
--
ALTER TABLE `audio_visual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dt_antrian`
--
ALTER TABLE `dt_antrian`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `md_pelayanan`
--
ALTER TABLE `md_pelayanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `permissions_groups`
--
ALTER TABLE `permissions_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
