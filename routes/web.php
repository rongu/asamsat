<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', function () {
    return redirect()->route('home');
})->middleware('auth');





Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home-grafik-list', [App\Http\Controllers\HomeController::class, 'grafiklist'])->name('home.grafik.list');

Route::get('/operator', [App\Http\Controllers\OperatorController::class, 'index'])->name('operator');
Route::get('/update-status-antrian/{id}/{tanggal}/{antrian}', [App\Http\Controllers\OperatorController::class, 'updateStatusAntrian'])->name('update.status.antrian');
Route::get('/prev-status-antrian/{id}/{tanggal}/{antrian}', [App\Http\Controllers\OperatorController::class, 'prevStatusAntrian'])->name('prev.status.antrian');

Route::get('/guest', [App\Http\Controllers\GuestController::class, 'index'])->name('guest');
Route::get('/guest-ambil-antrian/{id}/{tanggal}/{antrian}', [App\Http\Controllers\GuestController::class, 'guestAmbilAntrian'])->name('guest.ambil.antrian');
Route::get('/print/{id}/{tanggal}/{antrian}', [App\Http\Controllers\GuestController::class, 'print'])->name('print');

Route::get('/profil', [App\Http\Controllers\MyProfilesController::class, 'index'])->name('profil');
Route::post('/profil-create', [App\Http\Controllers\MyProfilesController::class, 'store'])->name('profil.create');

Route::get('/monitor', [App\Http\Controllers\MonitorController::class, 'index'])->name('monitor');
Route::get('/reload-monitor-antrian/{id}', [App\Http\Controllers\MonitorController::class, 'reloadMonitorAntrian'])->name('reload.mmonitor.antrian');

Route::get('/users-page', [App\Http\Controllers\UsersController::class, 'index'])->middleware('permission:users.page')->name('users.page');
Route::get('/users-list', [App\Http\Controllers\UsersController::class, 'usersList'])->name('users.list');
Route::post('/users-create', [App\Http\Controllers\UsersController::class, 'store'])->name('users.create');
Route::get('/users-delete/{id}', [App\Http\Controllers\UsersController::class, 'destroy'])->name('users.delete');

Route::get('/rekap-page', [App\Http\Controllers\RekapController::class, 'index'])->middleware('permission:rekap.page')->name('rekap.page');
Route::post('/rekap-list', [App\Http\Controllers\RekapController::class, 'rekapList'])->name('rekap.list');

Route::get('/setup-page', [App\Http\Controllers\SetupController::class, 'index'])->name('setup.page');
Route::post('/setup-update', [App\Http\Controllers\SetupController::class, 'update'])->name('setup.update');

Route::get('/privilege-page', [App\Http\Controllers\PrivilegeController::class, 'index'])->middleware('permission:privilege.page')->name('privilege.page');
Route::get('/privilege-list', [App\Http\Controllers\PrivilegeController::class, 'privilegeList'])->name('privilege.list');
Route::post('/privilege-create', [App\Http\Controllers\PrivilegeController::class, 'store'])->name('privilege.create');
Route::get('/privilege-delete/{id}', [App\Http\Controllers\PrivilegeController::class, 'destroy'])->name('privilege.delete');

Route::get('/go-apps', [App\Http\Controllers\ApplicationController::class, 'index'])->name('go.apps');
Route::get('/apps-reg-history', [App\Http\Controllers\EmployeesController::class, 'regHistory'])->name('go.apps.regHistory');
Route::get('/apps-history', [App\Http\Controllers\ApplicationController::class, 'history'])->name('go.apps.history');
Route::get('/apps-profile', [App\Http\Controllers\ApplicationController::class, 'profile'])->name('go.apps.profile');
Route::post('/apps-update-profile', [App\Http\Controllers\ApplicationController::class, 'updateProfile'])->name('go.apps.updateProfile');
Route::post('/apps-update-password', [App\Http\Controllers\ApplicationController::class, 'updatePassword'])->name('go.apps.updatePassword');
Route::get('/apps-antrian', [App\Http\Controllers\ApplicationController::class, 'antrian'])->name('go.apps.antrian');
Route::get('/apps-ambil-antrian', [App\Http\Controllers\ApplicationController::class, 'ambilAntrian'])->name('go.apps.ambilAntrian');
Route::post('/apps-antrian-create', [App\Http\Controllers\ApplicationController::class, 'antrianCreate'])->name('go.apps.antrianCreate');
Route::get('/apps-setting', [App\Http\Controllers\ApplicationController::class, 'setting'])->name('go.apps.setting');
Route::get('/apps-antrian-ready/{jns}', [App\Http\Controllers\ApplicationController::class, 'antrianReady'])->name('go.apps.ready');

Route::get('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

Route::get('/sendmail','SendMailController@index');