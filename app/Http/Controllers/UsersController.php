<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\User;
use \App\Models\RoleUser;
use \App\Models\Roles;
use DB,Session,Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //

        $rl = Roles::get();
        $userna = User::select('users.id','users.name','users.email','employee.phones','employee.nik','roles.name as rolename')->leftjoin('role_user', 'role_user.user_id', 'users.id')->leftjoin('roles', 'roles.id', 'role_user.role_id')->leftjoin('employee', 'employee.users_id', 'users.id')->whereNull('roles.name')->get();
        return view('users.index',compact('rl','userna'));
    }

    public function usersList()
    {
        //

        $codena = User::select('users.id','users.name','users.email','users.phone','users.nik','users.alamat','roles.name as rolename','roles.id as roleid')->leftjoin('role_user', 'role_user.user_id', 'users.id')->leftjoin('roles', 'roles.id', 'role_user.role_id')->get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('name', function($codena) {

                return $codena->name;

            })->addColumn('email', function($codena) {

                return $codena->email;                

            })->addColumn('phone', function($codena) {

                return $codena->phone;                

            })->addColumn('roles', function($codena) {

                return $codena->rolename;                

            })->addColumn('users_id', function($codena) {

                return [$codena->id,$codena->name,$codena->email,$codena->phone,$codena->nik,$codena->alamat,$codena->rolename,$codena->roleid];                

            })->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $users_id = $request->id;
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $nik = $request->nik;
        $alamat = $request->alamat;
        $password = $request->password;
        $roles_id = $request->roles_id;
        
        
        $user_ex = User::where('id', $users_id)->exists();


        if($user_ex){

            if (Auth::user()->hasPermission('users.edit')) { // you can pass an id or slug
                    
                $nclt = User::findOrFail($users_id);
                $nclt->name = $name;
                $nclt->email = $email;  
                $nclt->phone = $phone;  
                $nclt->nik = $nik;  
                $nclt->alamat = $alamat;
                if($password){
                    $nclt->password = Hash::make($password);  
                }
                
                $nclt->update();

                $role_ex = RoleUser::where('user_id', $users_id)->first();

                $rs = RoleUser::findOrFail($role_ex->id);
                $rs->role_id = $roles_id;
                $rs->user_id = $nclt->id;
                $rs->update();

                    Session::flash('update', 'Data User Telah di Ubah');

            }else{
                    Session::flash('error', 'User Tidak Memiliki akses Ubah');
            }   



        }else{

            if (Auth::user()->hasPermission('users.add')) { // you can pass an id or slug

                $nclt = new User();
                $nclt->name = $name;
                $nclt->email = $email;  
                $nclt->phone = $phone;  
                $nclt->nik = $nik;  
                $nclt->alamat = $alamat;  
                $nclt->password = Hash::make($password);  
                $nclt->save();

                $rs = new RoleUser();
                $rs->role_id = $roles_id;
                $rs->user_id = $nclt->id;
                $rs->save();

                Session::flash('success', 'Data User Telah di Simpan');
            }else{
                Session::flash('error', 'User Tidak Memiliki akses Tambah');
            }  


        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        if (Auth::user()->hasPermission('users.delete')) { // you can pass an id or slug
            $userx = RoleUser::where('user_id', $id)->first();
            RoleUser::where('id', $userx->id)->delete();


            $data = true;
        }else{
            $data = false;
            Session::flash('error', 'User Tidak Memiliki akses Hapus');
        }


        return response()->json($data);
    }
}
