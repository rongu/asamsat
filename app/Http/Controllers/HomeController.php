<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use \App\Models\RegHistory;
use \App\Models\MdPelayanan;
use \App\Models\User;
use \App\Models\RoleUser;
use Session,DB,Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $total_pengguna = User::leftjoin('role_user', 'role_user.user_id', 'users.id')->where('role_user.role_id','2')->count('users.id');
        $total_harian = RegHistory::where('tanggal_pelayanan',date('Y-m-d'))->count('id');
        $total_selesai = RegHistory::where('status_pelayanan','SELESAI')->where('tanggal_pelayanan',date('Y-m-d'))->count('id');
        $total_sisa = RegHistory::where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->count('id');

        $role = RoleUser::where('user_id',auth()->user()->id)->first();

        if ($role->role_id === 2) {
            return redirect()->route('go.apps');
        }else if ($role->role_id === 4){
            return redirect()->route('operator');
        }else if ($role->role_id === 3){
            return redirect()->route('guest');
        }


        return view('home',compact('total_pengguna','total_harian','total_selesai','total_sisa'));
    }

    public function grafiklist(){
        $data = DB::select( DB::raw("select dd.* from (select tanggal_pelayanan,count(id) as total from dt_antrian where tanggal_pelayanan < '".date('Y-m-d')."' group by tanggal_pelayanan order by tanggal_pelayanan desc limit 15) dd order by dd.tanggal_pelayanan asc") );

        $arr = '';

        $resultdata = array();

        foreach ($data as $keylue) {
            $arr = [$keylue->tanggal_pelayanan,$keylue->total];

            $resultdata[] = $arr;
        }

        return response()->json(json_encode(array_values($resultdata)));
    } 
}
