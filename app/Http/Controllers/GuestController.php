<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use \App\Models\RegHistory;
use \App\Models\MdPelayanan;
use \App\Models\User;
use \App\Models\RoleUser;
use Session,DB,Auth;

class GuestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set('Asia/Jakarta');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sim_baru = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MAX(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','1')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $sim_perpanjang = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MAX(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','2')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $skck_baru = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MAX(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','3')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $skck_perpanjang = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MAX(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','4')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $sidik_jari = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MAX(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','5')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        
        return view('guest',compact('sidik_jari','sim_baru','sim_perpanjang','skck_baru','skck_perpanjang'));
    }

    public function guestAmbilAntrian($id,$tanggal,$antrian)
    {
        //
        $jenis_pelayanan = $id;
        $tanggal_pelayanan = date('Y-m-d',strtotime(now()));
        $users_id = Auth::user()->id;

        $kode = MdPelayanan::where('id',$jenis_pelayanan)->first();

        $max = RegHistory::where('md_pelayanan_id',$jenis_pelayanan)->where('tanggal_pelayanan',$tanggal_pelayanan)->count('id');
        $max_nomor = $max + 1;

        $add_antrian = new RegHistory();
        $add_antrian->md_pelayanan_id = $jenis_pelayanan;
        $add_antrian->users_id = $users_id;
        $add_antrian->tanggal_pelayanan = $tanggal_pelayanan;
        $add_antrian->nomor_antrian = $max_nomor;
        $add_antrian->save();     

        $balikan = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MAX(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'),'md_pelayanan.nama_pelayanan')->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id',$id)->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.nama_pelayanan')->first();

        $urut = ApplicationController::getUrut($balikan->nomor_antrian);

        $result= array('kode_antrian' => $balikan->kode_antrian.' - '.$urut,'total' => $balikan->total,'md_pelayanan_id' => $balikan->md_pelayanan_id,'tanggal_pelayanan' => $balikan->tanggal_pelayanan, 'nomor_antrian'=>$balikan->nomor_antrian, 'nama_pelayanan'=>$balikan->nama_pelayanan);

        return response()->json($result);
    }

    public function print($id,$tanggal,$antrian)
    {
        return view('print.tiket');
    }
    
}
