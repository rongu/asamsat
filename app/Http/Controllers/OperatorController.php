<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use \App\Models\RegHistory;
use \App\Models\MdPelayanan;
use \App\Models\User;
use \App\Models\RoleUser;
use App\Mail\NotifMail;
use Illuminate\Support\Facades\Mail;
use Session,DB,Auth;

class OperatorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sim_baru = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','1')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $sim_perpanjang = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','2')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $skck_baru = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','3')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $skck_perpanjang = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','4')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $sidik_jari = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','5')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        
        return view('operator',compact('sidik_jari','sim_baru','sim_perpanjang','skck_baru','skck_perpanjang'));
    }

    public function prevStatusAntrian($id,$tanggal,$antrian){

        $urut = $antrian - 1;

        RegHistory::where('md_pelayanan_id',$id)->where('tanggal_pelayanan',$tanggal)->where('nomor_antrian',$urut)->update(['status_pelayanan'=>'MENUNGGU']);

        $balikan = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id',$id)->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();

        $urut = ApplicationController::getUrut($balikan->nomor_antrian);

        $result= array('kode_antrian' => $balikan->kode_antrian.' - '.$urut,'total' => $balikan->total,'md_pelayanan_id' => $balikan->md_pelayanan_id,'tanggal_pelayanan' => $balikan->tanggal_pelayanan, 'nomor_antrian'=>$balikan->nomor_antrian);

        return response()->json($result);
    }

    public function updateStatusAntrian($id,$tanggal,$antrian){

        RegHistory::where('md_pelayanan_id',$id)->where('tanggal_pelayanan',$tanggal)->where('nomor_antrian',$antrian)->update(['status_pelayanan'=>'SELESAI']);

        $balikan = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id',$id)->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();

        $urut = ApplicationController::getUrut($balikan->nomor_antrian);

        $result= array('kode_antrian' => $balikan->kode_antrian.' - '.$urut,'total' => $balikan->total,'md_pelayanan_id' => $balikan->md_pelayanan_id,'tanggal_pelayanan' => $balikan->tanggal_pelayanan, 'nomor_antrian'=>$balikan->nomor_antrian);

        $next_antrian = $antrian + 2;
        $next_urut = ApplicationController::getUrut($next_antrian);

        $notif = RegHistory::select('md_pelayanan.nama_pelayanan','md_pelayanan.kode_antrian','dt_antrian.nomor_antrian','users.name','users.phone','users.email')->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->leftjoin('users', 'users.id', 'dt_antrian.users_id')->leftjoin('role_user', 'role_user.user_id', 'dt_antrian.users_id')->where('dt_antrian.md_pelayanan_id',$id)->where('dt_antrian.tanggal_pelayanan',date('Y-m-d'))->where('role_user.role_id','2')->where('dt_antrian.nomor_antrian',$next_antrian)->first();

        Mail::to($notif->email)->send(new NotifMail($notif->nama_pelayanan,$notif->kode_antrian,$next_urut,$notif->name));

        $cek_id_phone = substr($notif->phone, 0, 1);

        if ($cek_id_phone == '0'){
            $nom = '62'.substr($notif->phone, 1);
            $url = "https://api.whatsapp.com/send?phone=".$nom."&text=Nomor%20antrian%20anda%20akan%20segera%20dilayani.%20Silahkan%20tunggu%20di%20tempat%20yang%20sudah%20disediiakan.%20Terima%20kasih.";
        }else{
            $url = "https://api.whatsapp.com/send?phone=".$notif->phone."&text=Nomor%20antrian%20anda%20akan%20segera%20dilayani.%20Silahkan%20tunggu%20di%20tempat%20yang%20sudah%20disediiakan.%20Terima%20kasih.";
        }

        //return Redirect::to($url);
        $data = array('user'=>$result,'url'=>$url);
        return response()->json($data);
    }

}
