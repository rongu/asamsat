<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\User;
use \App\Models\RoleUser;
use \App\Models\Roles;
use \App\Models\RegHistory;
use \App\Models\MdPelayanan;
use DB,Session,Auth;
use Illuminate\Support\Facades\Hash;

class RekapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $jpl = DB::table('md_pelayanan')->get(); 
        return view('rekap.index',compact('jpl'));
    }

    public function rekapList(Request $request)
    {
        $tanggal = $request->tanggal_pelayanan;
        $jenis = $request->jenis_pelayanan;


        $codena  = RegHistory::select('dt_antrian.tanggal_pelayanan','users.name','users.phone','dt_antrian.md_pelayanan_id','md_pelayanan.nama_pelayanan','md_pelayanan.kode_antrian','dt_antrian.nomor_antrian','dt_antrian.status_pelayanan')->leftjoin('users', 'users.id', 'users_id')->leftjoin('md_pelayanan', 'md_pelayanan.id', 'dt_antrian.md_pelayanan_id')->whereDate('dt_antrian.tanggal_pelayanan', '=', date('Y-m-d',strtotime($tanggal)));
        
        if ($jenis != 0){
            $codena = $codena->where('dt_antrian.md_pelayanan_id',$jenis);
        }
        
        $codena = $codena->get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('nama', function($codena) {

                return $codena->name;                

            })->addColumn('telepon', function($codena) {

                return $codena->phone;                

            })->addColumn('nama_pelayanan', function($codena) {

                return $codena->nama_pelayanan;                

            })->addColumn('nomor_antrian', function($codena) {

                return $codena->nomor_antrian;

            })->addColumn('status_pelayanan', function($codena) {

                return $codena->status_pelayanan;

            })->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $users_id = $request->id;
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $nik = $request->nik;
        $alamat = $request->alamat;
        $password = $request->password;
        $roles_id = $request->roles_id;
        
        
        $user_ex = User::where('id', $users_id)->exists();


        if($user_ex){

            if (Auth::user()->hasPermission('users.edit')) { // you can pass an id or slug
                    
                $nclt = User::findOrFail($users_id);
                $nclt->name = $name;
                $nclt->email = $email;  
                $nclt->phone = $phone;  
                $nclt->nik = $nik;  
                $nclt->alamat = $alamat;
                if($password){
                    $nclt->password = Hash::make($password);  
                }
                
                $nclt->update();

                $role_ex = RoleUser::where('user_id', $users_id)->first();

                $rs = RoleUser::findOrFail($role_ex->id);
                $rs->role_id = $roles_id;
                $rs->user_id = $nclt->id;
                $rs->update();

                    Session::flash('update', 'Data User Telah di Ubah');

            }else{
                    Session::flash('error', 'User Tidak Memiliki akses Ubah');
            }   



        }else{

            if (Auth::user()->hasPermission('users.add')) { // you can pass an id or slug

                $nclt = new User();
                $nclt->name = $name;
                $nclt->email = $email;  
                $nclt->phone = $phone;  
                $nclt->nik = $nik;  
                $nclt->alamat = $alamat;  
                $nclt->password = Hash::make($password);  
                $nclt->save();

                $rs = new RoleUser();
                $rs->role_id = $roles_id;
                $rs->user_id = $nclt->id;
                $rs->save();

                Session::flash('success', 'Data User Telah di Simpan');
            }else{
                Session::flash('error', 'User Tidak Memiliki akses Tambah');
            }  


        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        if (Auth::user()->hasPermission('users.delete')) { // you can pass an id or slug
            $userx = RoleUser::where('user_id', $id)->first();
            RoleUser::where('id', $userx->id)->delete();


            $data = true;
        }else{
            $data = false;
            Session::flash('error', 'User Tidak Memiliki akses Hapus');
        }


        return response()->json($data);
    }
}
