<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use \App\Models\RegHistory;
use \App\Models\MdPelayanan;
use \App\Models\User;
use \App\Models\RoleUser;
use \App\Models\Av;
use Session,DB,Auth;

class MonitorController extends Controller
{
    public function index()
    {

        $sim_baru = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','1')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $sim_perpanjang = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','2')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $skck_baru = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','3')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $skck_perpanjang = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','4')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $sidik_jari = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','5')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();
        $av  = Av::first();
        
        return view('monitor',compact('sidik_jari','sim_baru','sim_perpanjang','skck_baru','skck_perpanjang','av'));
    }

    public function reloadMonitorAntrian($id){

        $balikan = RegHistory::select('dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id',$id)->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian','dt_antrian.md_pelayanan_id','dt_antrian.tanggal_pelayanan')->first();

        if($balikan){
            $urut = ApplicationController::getUrut($balikan->nomor_antrian);

            $result= array('kode_antrian' => $balikan->kode_antrian.' - '.$urut);
        }else{
            $result= array('kode_antrian' => "-");
        }

        

        return response()->json($result);
    }
    
}
