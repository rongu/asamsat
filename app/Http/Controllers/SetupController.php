<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use \App\Models\MdPelayanan;
use Session,DB,Auth;

class SetupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set('Asia/Jakarta');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $in_p_s_b = MdPelayanan::select('batasan')->where('id',1)->first();
        $in_p_s_p = MdPelayanan::select('batasan')->where('id',2)->first();
        $in_p_sk_b = MdPelayanan::select('batasan')->where('id',3)->first();
        $in_p_sk_p = MdPelayanan::select('batasan')->where('id',4)->first();
        $in_p_r_s = MdPelayanan::select('batasan')->where('id',5)->first();

        return view('setup.index',compact('in_p_s_b','in_p_s_p','in_p_sk_b','in_p_sk_p','in_p_r_s'));
    }

    public function update(Request $request)
    {

        if($request->p_s_b){
            $nclt = MdPelayanan::findOrFail(1);
            $nclt->batasan = $request->p_s_b;  
            $nclt->update();
        }
        
        
        
        if($request->p_s_p){
            $nclt = MdPelayanan::findOrFail(2);
            $nclt->batasan = $request->p_s_p;  
            $nclt->update();
        }

        
        
        if($request->p_sk_b){
            $nclt = MdPelayanan::findOrFail(3);
            $nclt->batasan = $request->p_sk_b;  
            $nclt->update();
        }

        
        
        if($request->p_sk_p){
            $nclt = MdPelayanan::findOrFail(4);
            $nclt->batasan = $request->p_sk_p;  
            $nclt->update();
        }

        
        if($request->p_r_s){
            $nclt = MdPelayanan::findOrFail(5);
            $nclt->batasan = $request->p_r_s;  
            $nclt->update();
        }
        

        Session::flash('ubahsetup', 'Batasan Telah Diubah');  

        return redirect()->back();
    }
}