<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Roles;
use \App\Models\PermissionRole;
use Session, DB, Auth;

class PrivilegeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $access = DB::table('permissions_groups')->select('permissions.id as id','permissions.name as name')->leftjoin('permissions','permissions.id','permissions_groups.permissions_id')->get();
        return view('privilege.index',compact('access'));
    }

    public function privilegeList()
    {
        //

        $codena = Roles::get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('name', function($codena) {
                
               return $codena->name;

            })->addColumn('level', function($codena) {

                return $codena->level;                

            })->addColumn('access', function($codena) {

                return "access";                

            })->addColumn('roles_id', function($codena) {

                $permission = DB::table('permission_role')->where('role_id',$codena->id)->get();
                $ag = array();
                foreach ($permission as $kue) {
                    $ag[] = $kue->permission_id;
                }

                return [$codena->id,$codena->name,$codena->level,json_encode($ag)];

            })->toJson();
    }

    static public function getChild($id){
        $getchild = DB::table('permissions_groups')->select('childs_id')->where('permissions_id',$id)->first();
        if(is_array(json_decode($getchild->childs_id)))
        {

             $access = DB::table('permissions')->select('permissions.id as id','permissions.name as name')->whereIn('permissions.id',json_decode($getchild->childs_id,true))->get();

        }else{
            $access = array();   
        
        }
        return $access;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $roles_id = $request->id;
        $name_role = $request->name_role;
        $level_role = $request->level_role;
        $access_id = $request->access_id;

        if($roles_id==0){

             if (Auth::user()->hasPermission('privilege.add')) { // you can pass an id or slug
                
                $pjac = new Roles();
                $pjac->name = $name_role;
                $pjac->slug = str_replace(' ', '.', strtolower($name_role));
                $pjac->description = $name_role." Role";
                $pjac->level = $level_role;
                $pjac->save();

                $idrol = $pjac->id;

                if($access_id){
                    foreach ($access_id as $kue) {
                        //echo "-".$kue."<br>";
                        $rol = new PermissionRole();
                        $rol->role_id = $idrol;
                        $rol->permission_id = $kue;
                        $rol->save();

                    }    
                }
                

                Session::flash('success', 'Data Privilege Telah di Simpan');
            }else{

                Session::flash('error', 'User Tidak Memiliki akses Tambah');
            }  

        }else{

            if (Auth::user()->hasPermission('privilege.edit')) { // you can pass an id or slug
                 
                $pjac = Roles::findOrFail($roles_id);
                $pjac->name = $name_role;
                $pjac->slug = str_replace(' ', '.', strtolower($name_role));
                $pjac->description = $name_role." Role";
                $pjac->level = $level_role;

                $pjac->update();

                if($access_id){
                    
                    PermissionRole::where('role_id', $roles_id)->delete();

                    foreach ($access_id as $kue) {
                        //echo "-".$kue."<br>";
                        $rol = new PermissionRole();
                        $rol->role_id = $roles_id;
                        $rol->permission_id = $kue;
                        $rol->save();

                    }    
                }

                Session::flash('success', 'Data Privilege Telah di Ubah');  
            }else{
               Session::flash('error', 'User Tidak Memiliki akses Ubah');
            }  

        }



        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Auth::user()->hasPermission('privilege.delete')) { // you can pass an id or slug

                
            Roles::where('id', $id)->delete();
            PermissionRole::where('role_id', $id)->delete();
            $data = true;
        }else{
            $data = false;
            Session::flash('error', 'User Tidak Memiliki akses Hapus');
        }

        return response()->json($data);
    }
}
