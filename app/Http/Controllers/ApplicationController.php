<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use \App\Models\RegHistory;
use \App\Models\MdPelayanan;
use \App\Models\User;
use Illuminate\Support\Facades\Hash;
use Session,DB,Auth;


class ApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $profil = User::select(DB::raw('SUBSTRING(name, 1, 1) as inisial'))->where('id',Auth::user()->id)->first();    
        return view('apps.index',compact('profil'));
    }

    public function history()
    {
        $rehHistory = RegHistory::select('dt_antrian.id','md_pelayanan.nama_pelayanan','dt_antrian.tanggal_pelayanan','md_pelayanan.kode_antrian','dt_antrian.nomor_antrian','dt_antrian.status_pelayanan')->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('users_id',Auth::user()->id)->get();
        //$rehHistory = RegHistory::where('user_id',Auth::user()->id)->get();
        //$rehHistory = RegHistory::where('user_id',auth()->user()->id)->get();
        return view('apps.histori',compact('rehHistory'));
    }

    public function profile()
    {
        $profil = User::select('nik','name','email','phone','alamat',DB::raw('SUBSTRING(name, 1, 1) as inisial'))->where('id',Auth::user()->id)->first();    
        return view('apps.profile',compact('profil'));
    }

    public function antrian()
    {
        $sim_baru = RegHistory::select('md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','1')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian')->first();
        $sim_perpanjang = RegHistory::select('md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','2')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian')->first();
        $skck_baru = RegHistory::select('md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','3')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian')->first();
        $skck_perpanjang = RegHistory::select('md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','4')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian')->first();
        $sidik_jari = RegHistory::select('md_pelayanan.kode_antrian',DB::raw('MIN(dt_antrian.nomor_antrian) as nomor_antrian'),DB::raw('count(dt_antrian.id) as total'))->leftjoin('md_pelayanan', 'dt_antrian.md_pelayanan_id', 'md_pelayanan.id')->where('md_pelayanan_id','5')->where('status_pelayanan','MENUNGGU')->where('tanggal_pelayanan',date('Y-m-d'))->groupBy('kode_antrian')->first();
        
        return view('apps.nomor-antrian',compact('sim_baru','sim_perpanjang','skck_baru','skck_perpanjang','sidik_jari'));
    }

    public function ambilAntrian()
    {
        $jpl = DB::table('md_pelayanan')->get();    
        return view('apps.ambil-antrian',compact('jpl'));
    }   

    public function setting()
    {
        return view('apps.setting');
    }   
    
    static public function getUrut($nourut){
        $p_digit = strlen($nourut);
        $seturut = "000";
        $jml_urut = strlen($seturut);
        $geturut = substr_replace($seturut,$nourut,$jml_urut-$p_digit,$p_digit);
        return $geturut;
    }

    public function antrianCreate(Request $request)
    {
        //
        $jenis_pelayanan = $request->jenis_pelayanan;
        $tanggal_pelayanan = date('Y-m-d',strtotime($request->tanggal_pelayanan));
        $users_id = Auth::user()->id;

        $kode = MdPelayanan::where('id',$jenis_pelayanan)->first();

        $max = RegHistory::where('md_pelayanan_id',$jenis_pelayanan)->where('tanggal_pelayanan',$tanggal_pelayanan)->count('id');
        $max_nomor = $max + 1;

        $cek_pelayanan = RegHistory::where('md_pelayanan_id',$jenis_pelayanan)->where('tanggal_pelayanan',$tanggal_pelayanan)->where('users_id',$users_id)->count('id');

        if ($cek_pelayanan == 0) {
            $add_antrian = new RegHistory();
            $add_antrian->md_pelayanan_id = $jenis_pelayanan;
            $add_antrian->users_id = $users_id;
            $add_antrian->tanggal_pelayanan = $tanggal_pelayanan;
            $add_antrian->nomor_antrian = $max_nomor;
            $add_antrian->save();     

            Session::flash('success', 'Nomor Antrian Anda : '.$kode->kode_antrian.' - '.$max_nomor);   
        }else{
           Session::flash('error', 'Anda Sudah Melakukan Pendaftaran Pelayanan Ini.');
        }         
        
        return redirect()->back();
    }

    public function updateProfile(Request $request)
    {
        $nik = $request->nik;
        $nama = $request->nama;
        $alamat = $request->alamat;
        $telpon = $request->telpon;
        $email = $request->email;
        $users_id = Auth::user()->id;

        $nclt = User::findOrFail($users_id);
        $nclt->name = $nama;
        $nclt->email = $email;  
        $nclt->phone = $telpon;  
        $nclt->nik = $nik;  
        $nclt->alamat = $alamat;
        
        $nclt->update();

        Session::flash('update', 'Data Profil Anda Telah di Ubah');             
        
        return redirect()->back();
    } 

    public function updatePassword(Request $request)
    {
        $inputPassword1 = $request->inputPassword1;
        $inputPassword2 = $request->inputPassword2;
        $users_id = Auth::user()->id;

        if ($inputPassword1 == $inputPassword2) { // you can pass an id or slug
                
            $nclt = User::findOrFail($users_id);
            $nclt->password = Hash::make($inputPassword1);  
            $nclt->update();

            Session::flash('succes_update_password', 'Password Anda Berhasil Diubah');

        }else{
                Session::flash('error_konfirm', 'Password yang Anda Masukan Berbeda');
        }
        
        return redirect()->back();
    }

    public function antrianReady($jns){
        
        $bts = MdPelayanan::select('batasan')->where('id',$jns)->first();
        $date = date('Y-m-d',strtotime('+1 day'));
        $dta = RegHistory::where('md_pelayanan_id',$jns)->whereDate('tanggal_pelayanan',$date)->count('id');
        
        if($bts->batasan!=1){
            $jl = (new static)->getnextdate($date,$jns,1);
            //echo $jl;
            $date = date('Y-m-d',strtotime('+'.$jl.' day'));
            $date_s = $date;
        }else{
            $date_s = $date;
        }

        $result= array('jns' => $jns,'bts' => $bts->batasan, 'dt'=>$date, 'jml' => $dta, 'date_s' => $date_s);
        
        return response()->json($result);
    }

    static private function getnextdate($date,$jns,$num){
        $dta_get = RegHistory::where('md_pelayanan_id',$jns)->whereDate('tanggal_pelayanan',$date)->count('id');
        $bts_get = MdPelayanan::select('batasan')->where('id',$jns)->first();
        $cnt = $num;
        if($dta_get >= $bts_get->batasan){
            $cnt++;
            $jl = $cnt;
            $dateD = date('Y-m-d',strtotime('+'.$jl.' day'));
            $cnt = (new static)->getnextdate($dateD,$jns,$num+1);
            return $cnt;
        }else{
            return $num;
        }

        
    } 

}
