<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\User;
use Session,DB,Auth;

class MyProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $iduser = Auth::user()->id;
        $user = User::where('id',$iduser)->first();

        return view('profile.index',compact('user'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = $request->id;
        $nik_em = $request->nik_em;
        $name_em = $request->name_em;
        $email_em = $request->email_em;
        $phones_em = $request->phones_em;
        $address_em = $request->address_em;
        $password = $request->password_3;

        $empl = User::findOrFail($user_id);
        $empl->nik = $nik_em;
        $empl->name = $name_em;
        $empl->email = $email_em;
        $empl->phone = $phones_em;
        $empl->alamat = $address_em;
        $empl->update();

        $emplUser = User::findOrFail($user_id);
        if($password){
            $emplUser->password = Hash::make($password);
        }            
        
        $emplUser->update();

        Session::flash('success', 'Profil Telah di Ubah');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
