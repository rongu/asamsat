<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$username,$password)
    {
        //
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->username = $username;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@samsatbengkuluutara.com')
            ->view('mail/templatemail')
            ->with(['nama' => $this->name])
            ->with(['email' => $this->email])
            ->with(['username' => $this->username])
            ->with(['password' => $this->password])
            ->subject('Selamat Bergabung');            
    }
}
