<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nama_pelayanan,$kode_antrian,$next_urut,$name)
    {
        //
        $this->name = $name;
        $this->nama_pelayanan = $nama_pelayanan;
        $this->kode_antrian = $kode_antrian;
        $this->next_urut = $next_urut;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@samsatbengkuluutara.com')
            ->view('mail/notifemail')
            ->with(['nama' => $this->name])
            ->with(['nama_pelayanan' => $this->nama_pelayanan])
            ->with(['kode_antrian' => $this->kode_antrian])
            ->with(['next_urut' => $this->next_urut])
            ->subject('Pemberitahuan Antrian Pelayanaan Samsat Bengkulu Utara');            
    }
}
